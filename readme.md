# Chordal Distance Based PGO - A `g2o` Plugin
Pose-Graph optimization is a crucial component of many modern SLAM systems. Most prominent state of the art systems address this problem by iterative non-linear least squares. Both number of iterations and convergence basin of these approaches depend on the error functions used to describe the problem. The smoother and more convex the error function with respect to perturbations of the state variables is, the better the least-squares solver will perform.
We propose an alternative error function obtained by removing some non-linearities from the standard used one - i.e. the geodesic error function. Comparative experiments conducted on common benchmarking datasets confirm that our function is more robust to noise that affects the rotational component of the pose measurements and, thus, exhibits a larger convergence basin than the geodesic. Furthermore, its implementation is relatively easy w.r.t. the geodesic distance. This property leads to rather simple derivatives and nice numerical properties of the Jacobians resulting from the effective computation of the quadratic approximation used by Gauss-Newton algorithm.

### Getting started
This is a minimal invasive plugin for g2o that allows to use the chordal
distance for 3d pose graph optimization. The following instructions will allow to embed
it in your local g2o library and to run some benchmarks.

#### Prerequisites
1. Cholmod and CSparse libraries
```bash
sudo apt install libsuitesparse-dev
```
[WARNING] We noticed some strange behaviors of this library on `Ubuntu 18.04`. In particular, when using `cholmod` as `g2o` linear solver, the time per iteration inconsistent w.r.t. the values obtained on `Ubuntu 16.04`. We still ignore the causes of this behavior.

2. Working `g2o` library. If you don't have installed, you can download and follow the installation steps from [here](https://github.com/RainerKuemmerle/g2o).

#### Installation
1. Link the folder `plugin_se3_chordal` in your local folder `<g2o_root>/g2o/`

2. Add the following lines to the file `<g2o_root>/g2o/CMakeLists.txt` (at the end of the file)
```cmake
add_subdirectory(plugin_se3_chordal)
```

3. Clean build of the `g2o` library:
```bash
cd <g2o_root>/build
make clean
cmake ..
make -j4
```

### How to run
Once the plugin has been installed, you can use our custom vertices and edges in your software, just all the other `g2o` types. This plugin also embeds the chordal initialization of [Martinec et al.](https://ieeexplore.ieee.org/abstract/document/4270140).
Finally, the plugin will add some binaries to your current g2o library. Here you can find their description.

1. `geodesic_noise_adder3d`: allows to add to a standard g2o pose-graph - i.e. that uses standard `g2o` error function for `SE(3)` objects. The noise is AWGN and it is added on the translational and the rotational component of the `SE(3)` edges. **Input graph should be at the optimum**. To discover executable parameters, type:
```bash
./geodesic_noise_adder3d --help
```

2. `converter_geodesic2chordal`: converts a standard g2o pose-graph into a chordal g2o pose-graph. We suggest to use the default parameters for the Omega conversion. To discover executable parameters, type:
```bash
./converter_geodesic2chordal --help
```

3. `converter_chordal2geodesic`: converts chordal g2o pose-graph back into its geodesic formulation. To discover executable parameters, type:
```bash
./converter_chordal2geodesic --help
```

4. `g2o_comparator_app`: this app performs the optimization on a **chordal graph only**. It requires **also the standard formulation** of the input, in order to recompute at each iteration the chi2 using the standard error function. This can be used to perform unbiased `chi2` comparison between chordal and standard error function. To discover executable parameters, type:
```bash
./g2o_comparator_app --help
```

5. `g2o_chordal_app`: standard `g2o` batch optimization app, that logs custom statistics - e.g. they include also the `chi2` of the initial guess (`it= -1`). To discover the executable parameters, type:
```bash
./g2o_chordal_app -h
```

6. `ate_evaluator.cpp`: this utility allow to compute the Absolute Trajectory RMSE given a `SE(3)` pose-graph and its ground-truth. To discover the executable parameters, type
```bash
./ate_evaluator --help
```

### Results
Would you like to have some insights about how good this error function works? No problem, check the [wiki](https://gitlab.com/srrg-software/srrg_g2o_chordal_plugin/wikis/home)! There you will find all the plots that you want (☞ﾟヮﾟ)☞

### Authors
- Irvin Aloise
- Giorgio Grisetti

### Something isn't working?
[Contact the maintainer](mailto:ialoise@diag.uniroma1.it) or open an issue :)

### Related publications
Please cite our most recent article if you use our software:
```
@article{aloise2019chordal,
  title={Chordal Based Error Function for 3-D Pose-Graph Optimization},
  author={Aloise, Irvin and Grisetti, Giorgio},
  journal={IEEE Robotics and Automation Letters},
  volume={5},
  number={1},
  pages={274--281},
  year={2019},
  publisher={IEEE}
}
```

### License
BSD-4 clauses license

### Extra: Octave code
In the folder `octave` we provide a simple implementation of a LS optimization algorithm based on the chordal distance. It embeds also other kind of edges.
