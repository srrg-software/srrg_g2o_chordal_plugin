#include "edge_se3_chordal_init_translation.h"

namespace g2o {


  EdgeSE3ChordalInitTranslation::EdgeSE3ChordalInitTranslation():
         BaseBinaryEdge<3, Isometry3, VertexSE3ChordalInitTranslation, VertexSE3ChordalInitTranslation>() {
    _information.setIdentity();
  }

  bool EdgeSE3ChordalInitTranslation::read(std::istream& is) {
    Vector7 meas = Vector7::Zero();
    for (uint8_t i = 0; i < 7; ++i) {
      is >> meas[i];
    }

    _measurement = internal::fromVectorQT(meas);

    for (int i = 0; i < _information.rows(); ++i) {
      for (int j = i; j < _information.cols(); ++j) {
        is >> _information(i,j);
        if (i != j)
          _information(j,i) = _information(i,j);
      }
    }

    return true;
  }


  bool EdgeSE3ChordalInitTranslation::write(std::ostream& os) const {
    Vector7 meas = internal::toVectorQT(_measurement);
    for (uint8_t i = 0; i < 7; ++i) {
      os << meas[i] << " ";
    }

    for (int r = 0; r < _information.rows(); ++r) {
      for (int c = r; c < _information.cols(); ++c) {
        os << _information(r,c) << " ";
      }
    }
    return os.good();
  }


  void EdgeSE3ChordalInitTranslation::computeError() {
    VertexSE3ChordalInitTranslation* v_from = static_cast<VertexSE3ChordalInitTranslation*>(_vertices[0]);
    VertexSE3ChordalInitTranslation* v_to   = static_cast<VertexSE3ChordalInitTranslation*>(_vertices[1]);
    if (!v_from || !v_to) throw std::runtime_error("EdgeSE3TranslationInit::computeError|unexpected vertex type");
    _error.setZero();

    const Matrix3& Ri = v_from->estimate().linear();
    const Vector3& ti = v_from->estimate().translation();
    const Vector3& tj = v_to->estimate().translation();
    const Vector3& tij = _measurement.translation();

    _error = Ri*tij - tj + ti;
  }


  void EdgeSE3ChordalInitTranslation::linearizeOplus() {
    _jacobianOplusXi.setIdentity();
    _jacobianOplusXj = -_jacobianOplusXi;
  }

  bool EdgeSE3ChordalInitTranslation::setMeasurementFromState(){
    VertexSE3ChordalInitTranslation* from = static_cast<VertexSE3ChordalInitTranslation*>(_vertices[0]);
    VertexSE3ChordalInitTranslation* to   = static_cast<VertexSE3ChordalInitTranslation*>(_vertices[1]);
    Isometry3 delta = from->estimate().inverse() * to->estimate();
    setMeasurement(delta);
    return true;
  }


} /* namespace g2o */
