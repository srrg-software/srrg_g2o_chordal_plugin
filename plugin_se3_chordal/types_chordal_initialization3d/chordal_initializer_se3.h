#pragma once
//ia include fancy colors
#include "g2o/stuff/color_macros.h"

//ia include types
#include "g2o/types/slam3d/types_slam3d.h"
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include "g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h"
#include "g2o/plugin_se3_chordal/types_chordal_initialization3d/types_chordal_initialization3d.h"

//ia include g2o core stuff
#include "g2o/stuff/command_args.h"
#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/factory.h"
#include "g2o/core/block_solver.h"
#include "g2o/solvers/cholmod/linear_solver_cholmod.h"
#include "g2o/solvers/csparse/linear_solver_csparse.h"
#include "g2o/core/optimization_algorithm.h"
#include "g2o/core/optimization_algorithm_gauss_newton.h"

namespace g2o {

  class G2O_TYPES_SLAM3D_API ChordalInitializerSE3 {
  public:

    //! @brief we use gn_cholmod_var as optimization algorithm
    using InitilizationBlockSolver = g2o::BlockSolver<g2o::BlockSolverTraits<-1, -1>>;
    using InitilizationLinearSolverCholmod = g2o::LinearSolverCholmod<InitilizationBlockSolver::PoseMatrixType>;
    using InitilizationLinearSolverCSparse = g2o::LinearSolverCSparse<InitilizationBlockSolver::PoseMatrixType>;

    ChordalInitializerSE3() {}
    virtual ~ChordalInitializerSE3() {}

    //! @brief static method to initialize a SE3 Pose Graph
    //!        it's a long function that does the job.
    static void initializeGraph(SparseOptimizer& graph_, const bool& verbose_ = false);

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} /* namespace g2o */

