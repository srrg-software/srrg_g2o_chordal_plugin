#pragma once
#include "g2o/config.h"
#include "g2o/core/base_vertex.h"
#include "g2o/core/hyper_graph_action.h"
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include "g2o/types/slam3d/g2o_types_slam3d_api.h"
#include "g2o/plugin_se3_chordal/types_chordal3d/chordal_mappings.h"

namespace g2o {
  class G2O_TYPES_SLAM3D_API VertexSE3ChordalInitTranslation: public BaseVertex<3, Isometry3> {
  public:
    VertexSE3ChordalInitTranslation();
    virtual ~VertexSE3ChordalInitTranslation() {}


    void setToOriginImpl() override {
      _estimate = Isometry3::Identity();
    }

    bool read(std::istream& is) override;
    bool write(std::ostream& os) const override;

    bool setEstimateDataImpl(const number_t* est) override{
      Eigen::Map<const Vector7> v(est);
      _estimate=internal::fromVectorQT(v);
      return true;
    }

    bool getEstimateData(number_t* est) const override {
      Eigen::Map<Vector7> v(est);
      v=internal::toVectorQT(_estimate);
      return true;
    }

    int estimateDimension() const override {
      return 7;
    }

    bool setMinimalEstimateDataImpl(const number_t* est) override {
      Eigen::Map<const Vector6> v(est);
      _estimate = internal::fromVectorMQT(v);
      return true;
    }

    bool getMinimalEstimateData(number_t* est) const override {
      Eigen::Map<Vector6> v(est);
      v = internal::toVectorMQT(_estimate);
      return true;
    }

    int minimalEstimateDimension() const  override {
      return 6;
    }


    void oplusImpl(const number_t* update) override {
      //ia read the update
//      std::cerr << "VertexSE3ChordInit::oplusImpl|initial estimate of vertex" << _id << "\n" << _estimate.matrix() << std::endl;
      Eigen::Map<const Vector3> v(update);
      _estimate.translation() += v;
//      std::cerr << "VertexSE3ChordInit::oplusImpl|final estimate\n" << _estimate.matrix() << std::endl;
    }

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} /* namespace g2o */

