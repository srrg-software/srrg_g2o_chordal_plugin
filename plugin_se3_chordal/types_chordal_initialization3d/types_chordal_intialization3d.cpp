#include "types_chordal_initialization3d.h"
#include "g2o/core/factory.h"
#include "g2o/stuff/macros.h"

#include <iostream>

namespace g2o {
  //ia group
  G2O_REGISTER_TYPE_GROUP(chordal3d);

  //ia chordal initialization
  G2O_REGISTER_TYPE(VERTEX_SE3:CHORDINIT, VertexSE3ChordalInitRotation);
  G2O_REGISTER_TYPE(EDGE_SE3:CHORDINIT, EdgeSE3ChordalInitRotation);
  G2O_REGISTER_TYPE(VERTEX_SE3:TRANSINIT, VertexSE3ChordalInitTranslation);
  G2O_REGISTER_TYPE(EDGE_SE3:TRANSINIT, EdgeSE3ChordalInitTranslation);

} //ia end namespace g2o
