#include <iostream>
#include <map>
#include <string>

// ia include fancy colors
#include "g2o/stuff/color_macros.h"

// ia include types
#include "g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h"
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include "g2o/types/slam3d/types_slam3d.h"

// ia include g2o core stuff
#include "g2o/core/block_solver.h"
#include "g2o/core/factory.h"
#include "g2o/core/sparse_optimizer.h"
#include "g2o/stuff/command_args.h"

// ia include unscented
#include "g2o/stuff/unscented.h"

#include <Eigen/Core>
#include <Eigen/StdVector>

//! @brief converts a graph with standard EDGE_SE3:QUAT into chordal EDGE_SE3:CHORD ones;

using namespace std;
using namespace g2o;

// ia useful typedefs
typedef std::pair<int, int> IntPair;
typedef SigmaPoint<Vector6> SigmaPoint6;
typedef SigmaPoint<Vector12> SigmaPoint12;

typedef std::vector<SigmaPoint<Vector6>, Eigen::aligned_allocator<SigmaPoint<Vector6>>>
  SigmaPoint6Vector;
typedef std::vector<SigmaPoint<Vector12>, Eigen::aligned_allocator<SigmaPoint<Vector12>>>
  SigmaPoint12Vector;

enum OmegaConditioningType {SVDAddThresh=0x00,SVDAddAll=0x01,SVDThreshBlock=0x02,SVD6Block=0x03,SVD6BlockNoEps=0x04};

// @brief omega remapper from 6D to 12D using Unscented Transform
Matrix12 remapInformationMatrix(const Vector6& src_mean_,
                                const Matrix6& src_omega_,
                                const OmegaConditioningType& conditioning_type_,
                                const double& thresh_);

//! @brief conditioner: reconstructs sigma from degenerate sigma
Matrix12 reconditionateSigma(const Matrix12& src_sigma_,
                             const OmegaConditioningType& type_,
                             const double& threshold_);
//! @brief conditioner: reconstructs omega from degenerate sigma
Matrix12 reconstructOmegaFromSigma(const Matrix12& src_sigma_,
                                   const OmegaConditioningType& type_,
                                   const double& threshold_);

int main(int argc, char* argv[]) {
  // factory setup
  VertexSE3* v                  = new VertexSE3();
  VertexSE3EulerPert* v_chord   = new VertexSE3EulerPert();
  EdgeSE3* e_standard           = new EdgeSE3();
  EdgeSE3ChordalErrorA* e_chord = new EdgeSE3ChordalErrorA();

  std::string chord_vertex_tag = Factory::instance()->tag(v_chord);
  std::string chord_edge_tag   = Factory::instance()->tag(e_chord);

  // arguments of the executable
  std::string outFilename, inputFilename;
  double omegaThreshold;
  std::string omegaConditioningType;
  g2o::CommandArgs arg;
  arg.param("o", outFilename, "", "output of the conversion");
  arg.param("omegaTresh",
            omegaThreshold,
            1e-1,
            "threshold used to remap the information matrix of the edges");
  arg.param("condType", omegaConditioningType, "SVDAddThresh", "possible choices: SVDAddThresh | SVDAddAll | SVDThreshBlock | SVD6Block | SVD6BlockNoEps");
  arg.paramLeftOver("graph-input", inputFilename, "", "graph file which will be processed");
  arg.parseArgs(argc, argv);

  //ia checkout conditioning type
  OmegaConditioningType conditioningType = OmegaConditioningType::SVDAddThresh;
  if (omegaConditioningType == "SVDAddThresh") {
    std::cerr << "conditioning type: " << CL_YELLOW("SVDAddThresh") << std::endl;
    conditioningType = OmegaConditioningType::SVDAddThresh;
  } else if (omegaConditioningType == "SVDAddAll") {
    std::cerr << "conditioning type: " << CL_YELLOW("SVDAddAll") << std::endl;
    conditioningType = OmegaConditioningType::SVDAddAll;
  } else if (omegaConditioningType == "SVDThreshBlock") {
    std::cerr << "conditioning type: " << CL_YELLOW("SVDThreshBlock") << std::endl;
    conditioningType = OmegaConditioningType::SVDThreshBlock;
  } else if (omegaConditioningType == "SVD6Block") {
    std::cerr << "conditioning type: " << CL_YELLOW("SVD6Block") << std::endl;
    conditioningType = OmegaConditioningType::SVD6Block;
  } else if (omegaConditioningType == "SVD6BlockNoEps") {
    std::cerr << "conditioning type: " << CL_YELLOW("SVD6BlockNoEps") << std::endl;
    conditioningType = OmegaConditioningType::SVD6BlockNoEps;
  } else {
    std::cerr << "invalid conditioning type, choose between"
              << "\n\tSVDAddThresh   -> adds the threshold value to sigma sv under threshold, then inverts whole sigma"
              << "\n\tSVDAddAll      -> adds the threshold value to ALL sigma sv, then inverts whole sigma"
              << "\n\tSVDThreshBlock -> finds the first sv under threshold, then inverts only the non collapsed block of sigma"
              << "\n\tSVD6Block      -> finds the first sv under threshold among the first 6 sv, then inverts only the non collapsed 6 block of sigma"
              << "\n\tSVD6BlockNoEps -> converts only the non null block amongs the first 6 sv more informative"
              << std::endl;
  }

  // loading the graph
  SparseOptimizer optimizer;
  cerr << "opening the file : " << inputFilename << endl;
  std::ifstream ifs(inputFilename.c_str());
  optimizer.load(ifs);

  // getting vertices and edges from the graph
  const HyperGraph::VertexIDMap& vertices = optimizer.vertices();
  const HyperGraph::EdgeSet& edges        = optimizer.edges();

  const int num_vertices = vertices.size();
  const int num_edges    = edges.size();

  cerr << "Loaded " << CL_GREEN(num_vertices) << " vertexes" << endl;
  cerr << "Loaded " << CL_GREEN(num_edges) << " edges" << endl;

  // new edges
  HyperGraph::EdgeSet chordal_edges;

  // convert the edges
  size_t cnt = 0;
  std::cerr << "conversion started" << std::endl;
  for (HyperGraph::Edge* eptr : edges) {
    float percentage = std::ceil((float) cnt++ / (float) edges.size() * 100);
    if ((int) percentage % 5 == 0)
      std::cerr << "\rcompleted " << percentage << "%" << std::flush;

    EdgeSE3* e           = dynamic_cast<EdgeSE3*>(eptr);
    const Isometry3& Z   = e->measurement();
    const Matrix6& omega = e->information();

    // convert things
    Vector6 z            = internal::toVectorMQT(Z);
    Matrix12 omega_chord = remapInformationMatrix(z, omega, conditioningType, omegaThreshold);

    EdgeSE3ChordalErrorA* chord_edge = new EdgeSE3ChordalErrorA();
    chord_edge->setMeasurement(Z);
    chord_edge->setInformation(omega_chord);
    chord_edge->vertices() = eptr->vertices();
    chordal_edges.insert(chord_edge);
  }
  std::cerr << "\n";

  cerr << "converted " << chordal_edges.size() << " edges" << endl;

  // write the vertices - which remain the same
  std::ofstream file_output_stream;
  if (outFilename != "-") {
    cerr << "Writing into " << outFilename << endl;
    file_output_stream.open(outFilename.c_str());
  } else {
    cerr << "writing to stdout" << endl;
  }

  std::ostream& fout = outFilename != "-" ? file_output_stream : cout;

  VertexSE3EulerPert vertex_euler;
  for (std::pair<const int, g2o::HyperGraph::Vertex*> vit : vertices) {
    VertexSE3* v = dynamic_cast<VertexSE3*>(vit.second);
    vertex_euler.setEstimate(v->estimate());
    vertex_euler.setId(v->id());
    vertex_euler.setFixed(v->fixed());

    fout << chord_vertex_tag << " " << vertex_euler.id() << " ";
    vertex_euler.write(fout);
    fout << endl;
    if (vertex_euler.fixed()) {
      fout << "FIX " << vertex_euler.id() << endl;
    }
  }

  for (HyperGraph::Edge* eptr : chordal_edges) {
    EdgeSE3ChordalErrorA* e = dynamic_cast<EdgeSE3ChordalErrorA*>(eptr);
    fout << chord_edge_tag << " " << e->vertex(0)->id() << " " << e->vertex(1)->id() << " ";
    e->write(fout);
    fout << endl;
  }

  cerr << "done!" << endl;

  // checkout things
  file_output_stream.close();

  for (HyperGraph::Edge* e : chordal_edges) {
    delete e;
  }

  delete v;
  delete v_chord;
  delete e_standard;
  delete e_chord;
}

Matrix12 remapInformationMatrix(const Vector6& src_mean_,
                                const Matrix6& src_omega_,
                                const OmegaConditioningType& reconditioning_type_,
                                const double& threshold_) {
  // ia computing sigma_12D
  Matrix6 src_sigma = src_omega_.inverse();
  Isometry3 T_mean  = internal::fromVectorMQT(src_mean_);

  Vector12 remapped_mean  = Vector12::Zero();
  Matrix12 remapped_sigma = Matrix12::Zero();

  SigmaPoint6Vector sigma_points_6D;
  SigmaPoint12Vector sigma_points_12D;

  Vector6 zero_mean = Vector6::Zero();
  if (!sampleUnscented(sigma_points_6D, zero_mean, src_sigma))
    throw std::runtime_error("remapInformationMatrix|bad things happened while sampling");

  int k = 1;
  for (int i = 0; i < src_mean_.size(); ++i) {
    int sample_plus_idx  = k++;
    int sample_minus_idx = k++;

    const Vector6& sample_6D_plus  = sigma_points_6D[sample_plus_idx]._sample;
    const Vector6& sample_6D_minus = sigma_points_6D[sample_minus_idx]._sample;

    Isometry3 T_plus  = internal::fromVectorMQT(sample_6D_plus);
    Isometry3 T_minus = internal::fromVectorMQT(sample_6D_minus);

    Vector12 sample_12D_plus  = internal::toFlatten(T_mean * T_plus);
    Vector12 sample_12D_minus = internal::toFlatten(T_mean * T_minus);

    SigmaPoint12 point_12D_plus(
      sample_12D_plus, sigma_points_6D[sample_plus_idx]._wi, sigma_points_6D[sample_plus_idx]._wp);
    SigmaPoint12 point_12D_minus(sample_12D_minus,
                                 sigma_points_6D[sample_minus_idx]._wi,
                                 sigma_points_6D[sample_minus_idx]._wp);
    sigma_points_12D.push_back(point_12D_plus);
    sigma_points_12D.push_back(point_12D_minus);
  }

  reconstructGaussian(remapped_mean, remapped_sigma, sigma_points_12D);

  // ia reconditioning the new covariance
  Matrix12 remapped_omega = Matrix12::Zero();
  
  switch (reconditioning_type_) {
  case OmegaConditioningType::SVDAddThresh:
  case OmegaConditioningType::SVDAddAll:
    {
      Matrix12 conditioned_sigma = reconditionateSigma(remapped_sigma,
                                                       reconditioning_type_,
                                                       threshold_);
      remapped_omega = conditioned_sigma.inverse();
      break;
    }
  case OmegaConditioningType::SVDThreshBlock:
  case OmegaConditioningType::SVD6Block:
  case OmegaConditioningType::SVD6BlockNoEps:
    {
      remapped_omega = reconstructOmegaFromSigma(remapped_sigma, reconditioning_type_, threshold_);
      break;
    }
  default:
    throw std::runtime_error("remapInformationMatrix|invalid conditioning type");
  }

  return remapped_omega;
} //ia end remapInformationMatrix
 

Matrix12 reconstructOmegaFromSigma(const Matrix12& src_sigma_,
                                   const OmegaConditioningType& type_,
                                   const double& threshold_) {
  //ia declare some stuff
  Matrix12 converted_omega = Matrix12::Zero();
  Vector12 new_singular_values = Vector12::Zero();

  //ia index of the first collapsed sv
  int bad_sv_index = 0;
  //ia max dimension of the block to consider in the inversion
  int max_index = -1;
  //ia rebuild omega using the block of this dimension
  int reconstruction_max_index = -1;

  //ia compute the svd
  Eigen::JacobiSVD<Matrix12> svd(src_sigma_, Eigen::ComputeThinU | Eigen::ComputeThinV);

  switch (type_) {
  case OmegaConditioningType::SVDThreshBlock:
    {
      max_index = converted_omega.rows();

      //ia find the first collapsed sv in the block that we want to invert
      for (int i = 0; i < max_index; ++i) {
        if (svd.singularValues()(i, i) < threshold_) {
          new_singular_values[i] = 0;
          if (!bad_sv_index)
            bad_sv_index = i;
        } else {
          new_singular_values[i] = svd.singularValues()(i, i);
        }
      }
      
      if (bad_sv_index) {
        reconstruction_max_index = bad_sv_index;
      } else {
        reconstruction_max_index = max_index;
      }
      
      break;
    }
  case OmegaConditioningType::SVD6Block:
    {
      max_index = 6;
      //ia find the first collapsed sv in the block that we want to invert
      for (int i = 0; i < max_index; ++i) {
        if (svd.singularValues()(i, i) < threshold_) {
          new_singular_values[i] = threshold_;
          if (!bad_sv_index)
            bad_sv_index = i;
        } else {
          new_singular_values[i] = svd.singularValues()(i, i);
        }
      }

      reconstruction_max_index = max_index;
      break;
    }
  case OmegaConditioningType::SVD6BlockNoEps:
    {
      max_index = 6;
      for (int i = 0; i < max_index; ++i) {
        if (svd.singularValues()(i,i) < threshold_) {
          if (!bad_sv_index) {
            bad_sv_index = i;
          }
        }
      }

      reconstruction_max_index = bad_sv_index;
      break;
    }
  default:
    throw std::runtime_error("reconstructOmegaFromSigma|unexpected conditioning type");
  }

  //ia invert only the block that is not ill
  for (int i = 0; i < reconstruction_max_index; ++i) {
    converted_omega.noalias() +=
      svd.matrixU().col(i) * (1.f / new_singular_values[i]) * svd.matrixU().col(i).transpose();
  }

  //  std::cerr << "\nsingular values = ";
  //  std::cerr << new_singular_values.transpose() << std::endl;
  //  std::cerr << "index of collapse = " << bad_sv_index << std::endl;
  //  std::cerr << "converted omega =\n" << converted_omega << std::endl;

  //  exit(0);

  //ia done
  return converted_omega;
}

Matrix12 reconditionateSigma(const Matrix12& src_sigma_,
                             const OmegaConditioningType& type_,
                             const double& threshold_) {
  Matrix12 conditioned_sigma = Matrix12::Zero();

  switch (type_) {
  case OmegaConditioningType::SVDAddThresh:
    {
      Eigen::JacobiSVD<Matrix12> svd(src_sigma_, Eigen::ComputeThinU | Eigen::ComputeThinV);
      double conditioned_eigenvalue = 1.0;
      // std::cerr << std::endl;
      // std::cerr << "singular values = ";
      // for (size_t u = 0; u < 12; ++u) {
      //   std::cerr << svd.singularValues()(u,u) << " ";
      // }
      // std::cerr << std::endl;
      // exit(0);

      for (int i = 0; i < 12; ++i) {
        //! best
        if (svd.singularValues()(i, i) < threshold_) {
          conditioned_eigenvalue = svd.singularValues()(i, i) + threshold_;
        } else {
          conditioned_eigenvalue = svd.singularValues()(i, i);
        }
        conditioned_sigma.noalias() +=
          conditioned_eigenvalue * svd.matrixU().col(i) * svd.matrixU().col(i).transpose();
      }
      // std::cin.get();
      break;
    }

    case OmegaConditioningType::SVDAddAll:
    {
      conditioned_sigma = src_sigma_;
      conditioned_sigma.diagonal().array() += threshold_; //! Avoids rank loss (right value)
      // conditioned_sigma.diagonal().array() += 1e-10; //! Avoids rank loss (this value will
      // restore original omegas +/-)
      break;
    }
    default:
      break;
  }
  return conditioned_sigma;
}
