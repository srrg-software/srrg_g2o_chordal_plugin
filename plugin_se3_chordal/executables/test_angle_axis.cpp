#include <iostream>
#include <iomanip>
#include "g2o/stuff/color_macros.h"
#include "g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h"
#include "g2o/plugin_se3_chordal/types_chordal_initialization3d/types_chordal_initialization3d.h"

//ia dl loader
#include "g2o/apps/g2o_cli/dl_wrapper.h"
#include "g2o/apps/g2o_cli/output_helper.h"
#include "g2o/apps/g2o_cli/g2o_common.h"

using namespace g2o;

#define NUM_STEPS 400

number_t myDeg2Rad(const number_t& degrees_) {
  return std::atan2(std::sin(degrees_*M_PI/180), std::cos(degrees_*M_PI/180));
}

int main(int argc, char **argv) {
  DlWrapper dl_types_wrapper;
  loadStandardTypes(dl_types_wrapper, argc, argv);

  //ia boxplus and boxminus
  Vector6 increment_angle_axis = Vector6::Zero();
  Isometry3 original_angle_axis = Isometry3::Identity();
  Isometry3 original_quaternion = Isometry3::Identity();
  for (size_t i = 0; i < NUM_STEPS; ++i) {
    int deg = i%360;
    const number_t angle = myDeg2Rad(deg);

    std::printf("step [\033[34m%04ld\033[0m]| ", i);
    Vector3 translation_increment = Vector3::Random();

    increment_angle_axis.head<3>() = translation_increment;
    Vector3 axis = Vector3::Random();
    // Vector3 axis = Vector3::UnitZ();
    axis.normalize();

    std::cerr << std::fixed << std::setprecision(8);
    std::cerr << "angle  = " << angle 
              << "\taxis = " << axis.transpose() << std::endl;


    AngleAxis angle_axis = AngleAxis(angle, axis);
    increment_angle_axis.tail<3>() = angle_axis.angle() * angle_axis.axis();

    //ia boxplussing
    Isometry3 perturbed_angle_axis = original_angle_axis * internal::fromVectorSAA(increment_angle_axis);
    Isometry3 delta = original_angle_axis.inverse() * perturbed_angle_axis;
    Vector6 delta_vector = internal::toVectorSAA(delta);

    Vector6 mismatch = increment_angle_axis - delta_vector;

    if (mismatch.norm() > 1e-6) {
      std::cerr << "increment_angle_axis ------- | " << CL_GREEN(increment_angle_axis.transpose()) << std::endl;
      std::cerr << "error angle axis ----------- | " << CL_YELLOW(delta_vector.transpose()) << std::endl;
      std::cerr << "increment_angle_axis - error | " << CL_RED(mismatch.transpose()) << std::endl;
      std::printf("mismatch norm --------------- | {%6f}\n", mismatch.norm());
//      throw std::runtime_error("cancro");
    }

    // std::cerr << "original_angle_axis\n" << original_angle_axis.matrix() << std::endl;
    // std::cerr << "perturbed_angle_axis angle axis\n" << perturbed_angle_axis.matrix() << std::endl;

    //ia quaternion
    Vector3 q = internal::toCompactQuaternion(angle_axis.toRotationMatrix());
    Vector6 increment_quaternion = Vector6::Zero();
    increment_quaternion.head<3>() = translation_increment;
    increment_quaternion.tail<3>() = q;

    Isometry3 perturbed_quaternion = original_quaternion * internal::fromVectorMQT(increment_quaternion);
    Isometry3 delta_quaternion = original_quaternion.inverse() * perturbed_quaternion;
    Vector6 delta_vector_quaternion = internal::toVectorMQT(delta_quaternion);
    Vector6 mismatch_quaternion = increment_quaternion - delta_vector_quaternion;
    
    // if (mismatch_quaternion.norm() > 1e-6) {
    //   std::cerr << "increment_quat ------- | " << CL_GREEN(increment_quaternion.transpose()) << std::endl;
    //   std::cerr << "error quat ----------- | " << CL_YELLOW(delta_vector_quaternion.transpose()) << std::endl;
    //   std::cerr << "increment_quat - error | " << CL_RED(mismatch_quaternion.transpose()) << std::endl;
    //   std::printf("mismatch norm --------- | {%6f}\n", mismatch_quaternion.norm());
    // }
    // std::cin.get();
  }
  
  

  return 0;
}


