#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cmath>

//ia include types
#include "g2o/types/slam3d/types_slam3d.h"
#include "g2o/types/slam2d/types_slam2d.h"
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include "g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h"
#include "g2o/plugin_se3_chordal/types_chordal_initialization3d/types_chordal_initialization3d.h"

//ia include g2o core stuff
#include "g2o/stuff/sampler.h"
#include "g2o/stuff/command_args.h"
#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/factory.h"

//ia dynamic loaders
#include "g2o/apps/g2o_cli/dl_wrapper.h"
#include "g2o/apps/g2o_cli/g2o_common.h"
#include "g2o/apps/g2o_cli/output_helper.h"

using namespace g2o;
const std::string exe_name("graph_noise_adder_3d");
#define LOG std::cerr << exe_name + "|"

// ia checks whether file exists
bool checkFile(const std::string& name_) {
  struct stat buffer;
  return (stat(name_.c_str(), &buffer) == 0);
}

//ia some usings
using GaussianSampler3 = g2o::GaussianSampler<Vector3, Matrix3>;

//ia add noise to pose
void addNoiseSE3(g2o::HyperGraph::Edge* edge_,
                 GaussianSampler3& sampler_t_,
                 GaussianSampler3& sampler_r_,
                 const Matrix6& omega_);

//ia add noise to points
void addNoiseR3(g2o::HyperGraph::Edge* edge_,
                GaussianSampler3& sampler_xyz_,
                const Matrix3& omega_);

int main(int argc, char** argv) {
  LOG << "this executable adds AWGN to 3D pose graph or a pose-landmark graph [for now]\n";
  LOG << "INPUT GRAPH SHOULD BE AT THE OPTIMUM\n\n";

  //ia registering all the types from the libraries
  g2o::DlWrapper dlTypesWrapper;
  loadStandardTypes(dlTypesWrapper, argc, argv);
  g2o::DlWrapper dlSolverWrapper;
  loadStandardSolver(dlSolverWrapper, argc, argv);

  //ia command line args
  bool param_use_random_seed = false;
  std::vector<number_t> param_noise_pose_t;
  std::vector<number_t> param_noise_pose_r;
  std::vector<number_t> param_noise_point;
  std::string param_output_file = "";
  std::string param_input_file = "";

  //ia parse command line
  g2o::CommandArgs arg;
  arg.param("o",
            param_output_file,
            "",
            "output graph (noisyfied)");
  arg.param("pointSigma",
            param_noise_point,
            std::vector<double>(),
            "semicolumn-spaced Sigma for the points - default: \"0.01;0.01;0.01\"");
  arg.param("poseSigma_t",
            param_noise_pose_t,
            std::vector<double>(),
            "semicolumn-spaced Sigma for the pose noise, trans - default: \"0.01;0.01;0.01\"");
  arg.param("poseSigma_r",
            param_noise_pose_r,
            std::vector<double>(),
            "semicolumn-spaced Sigma for the pose noise, rot - default: \"0.005;0.005;0.005\"");
  arg.param("randomSeed",
            param_use_random_seed,
            false,
            "use a random seed to sample noise");
  arg.paramLeftOver("inputGraph",
                    param_input_file,
                    "",
                    "input graph to be processed");
  arg.parseArgs(argc, argv);

  //ia check if input file exists
  if (!checkFile(param_input_file)) {
    throw std::runtime_error(exe_name + "|ERROR, input file [ " +
                             param_input_file + " ] does not exists");
  }


  //ia use default noise values if not set
  if (param_noise_pose_t.size() == 0) {
    LOG << "WARNING, using default Sigma for the pose (translation)\n";
    param_noise_pose_t.push_back(0.01);
    param_noise_pose_t.push_back(0.01);
    param_noise_pose_t.push_back(0.01);
  }

  if (param_noise_pose_r.size() == 0) {
    LOG << "WARNING, using default Sigma for the pose (rotation)\n";
    param_noise_pose_r.push_back(0.005);
    param_noise_pose_r.push_back(0.005);
    param_noise_pose_r.push_back(0.005);
  }

  if (param_noise_point.size() == 0) {
    LOG << "WARNING, using default Sigma for the points\n";
    param_noise_point.push_back(0.01);
    param_noise_point.push_back(0.01);
    param_noise_point.push_back(0.01);
  }

  //ia compute stddevs
  Matrix3 sigma_pose_t = Matrix3::Identity();
  Matrix3 sigma_pose_r = Matrix3::Identity();
  Matrix3 sigma_point  = Matrix3::Identity();

  for (size_t i = 0; i < (size_t)3; ++i) {
    sigma_pose_t(i,i) = std::pow(param_noise_pose_t[i], 2);
    sigma_pose_r(i,i) = std::pow(param_noise_pose_r[i], 2);
    sigma_point(i,i)  = std::pow(param_noise_point[i], 2);
  }
  
  //ia generate full omega for pose-pose and pose-points measurements according to noise statistics
  Matrix6 omega_pose_meas = Matrix6::Zero();
  omega_pose_meas.block<3,3>(0,0) = sigma_pose_t.inverse();
  omega_pose_meas.block<3,3>(3,3) = sigma_pose_r.inverse();

  Matrix3 omega_point_meas = Matrix3::Zero();
  omega_point_meas = sigma_point.inverse();

  //ia log something
  LOG << "noise sigma pose (translational)\n" << sigma_pose_t << std::endl;
  LOG << "noise sigma pose (rotational)\n" << sigma_pose_r << std::endl;
  LOG << "noise sigma point\n" << sigma_point << std::endl;
  LOG << "omega pose-pose\n" << omega_pose_meas << std::endl;
  LOG << "omega pose-point\n" << omega_point_meas << std::endl;

  //ia load the input graph
  LOG << "loading file [ " << param_input_file << " ]\n";
  g2o::SparseOptimizer optimizer;
  std::ifstream ifs(param_input_file.c_str());
  optimizer.load(ifs);

  const HyperGraph::VertexIDMap& src_vertices = optimizer.vertices();
  const HyperGraph::EdgeSet& src_edges = optimizer.edges();

  const size_t num_v = src_vertices.size();
  const size_t num_e = src_edges.size();
  LOG << "input file has [ "
      << num_v << " ] vertices -- [ "
      << num_e << " ] edges\n";

  //ia setup noise generators
  GaussianSampler3 sampler_pose_t;
  GaussianSampler3 sampler_pose_r;
  GaussianSampler3 sampler_point;
  sampler_pose_t.setDistribution(sigma_pose_t);
  sampler_pose_r.setDistribution(sigma_pose_r);
  sampler_point.setDistribution(sigma_point);

  //ia setup random seeds for the samplers
  if (param_use_random_seed) {
    LOG << "using random seeds to sample noise\n";

    std::vector<int> seeds(3);
    std::random_device randomizer;
    std::seed_seq seed_sequence{randomizer(), randomizer(), randomizer(), randomizer(), randomizer()};
    seed_sequence.generate(seeds.begin(), seeds.end());
    assert(seeds.size() == 3);

    for (size_t i = 0; i < seeds.size(); ++i) {
      LOG << "seed [ " << i << " ] = " << seeds[i] << std::endl;
    }

    if (!sampler_pose_t.seed(seeds[0])) {
      throw std::runtime_error(exe_name+"|ERROR, cannot set random seed");
    }
    if (!sampler_pose_r.seed(seeds[1])) {
      throw std::runtime_error(exe_name+"|ERROR, cannot set random seed");
    }
    if (!sampler_point.seed(seeds[2])) {
      throw std::runtime_error(exe_name+"|ERROR, cannot set random seed");
    }
  }

  //ia now we have to generate new noisy measurements
  size_t cnt = 0, num_se3_edges = 0, num_r3_edges = 0, num_discarded_edges = 0;
  for (HyperGraph::Edge* eptr : src_edges) {
    //ia process each edge differently TODO super shitty
    const std::string edge_tag = Factory::instance()->tag(eptr);
    if (edge_tag == "EDGE_SE3:QUAT") {
      //ia adding noise to pose pose edges
      addNoiseSE3(eptr, sampler_pose_t, sampler_pose_r, omega_pose_meas);
      ++num_se3_edges;
    } else if (edge_tag == "EDGE_SE3_TRACKXYZ") {
      //ia adding noise to pose point edges
      addNoiseR3(eptr, sampler_point, omega_point_meas);
      ++num_r3_edges;
    } else {
      ++num_discarded_edges;
    }

    float percentage = std::ceil((float)cnt++ / (float)num_e * 100.f);
    if ((int)percentage % 5 == 0) {
      std::cerr << "\r" << exe_name << "|processed " << percentage << "%" << std::flush;
    }
  }

  //ia log
  std::cerr << std::endl;
  LOG << "processed [ "
      << num_se3_edges << "/" << cnt << " ] se3 edges -- [ "
      << num_r3_edges << "/" << cnt << " ] r3 edges ---- [ "
      << num_discarded_edges << "/" << cnt << " ] discarded edges\n";

  //ia save and checkout
  if (param_output_file.empty()) {
    LOG << "WARNING, no output file specified, graph will be lost\n";
  } else {
    LOG << "saving output in [ " << param_output_file << " ]\n";
    optimizer.save(param_output_file.c_str());
  }

  LOG << "done!\n";
  return 0;
}

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
void addNoiseSE3(g2o::HyperGraph::Edge* edge_,
                 GaussianSampler3& sampler_t_,
                 GaussianSampler3& sampler_r_,
                 const Matrix6& omega_) {
  EdgeSE3* e = dynamic_cast<EdgeSE3*>(edge_);
  if (!e) {
    throw std::runtime_error(exe_name+"|ERROR, invalid cast to EdgeSE3");
  }

  //ia estimate GT edge
  VertexSE3* v_from = dynamic_cast<VertexSE3*>(e->vertices()[0]);
  VertexSE3* v_to = dynamic_cast<VertexSE3*>(e->vertices()[1]);
  assert(v_from && v_to);
  
  const Isometry3& T_from = v_from->estimate();
  const Isometry3& T_to = v_to->estimate();
  const Isometry3 Z_gt = T_from.inverse() * T_to;
  const g2o::Quaternion gt_quat(Z_gt.linear());
  const Vector3 gt_trans(Z_gt.translation());

  //! sample
  const Vector3 trans_noise = sampler_t_.generateSample();
  const Vector3 noise_quatXYZ = sampler_r_.generateSample();

  //ia compose quaternion
  number_t noise_qw = 1.0 - noise_quatXYZ.norm();
  if (noise_qw < 0) {
    noise_qw = 0.;
  }

  g2o::Quaternion rot_noise(noise_qw, noise_quatXYZ.x(), noise_quatXYZ.y(), noise_quatXYZ.z());
  rot_noise.normalize();

  const g2o::Quaternion noisy_quat = gt_quat * rot_noise;
  Isometry3 noisy_meas = Isometry3::Identity();
  noisy_meas.linear() = noisy_quat.matrix();
  noisy_meas.translation() = gt_trans + trans_noise;

  e->setMeasurement(noisy_meas);
  e->setInformation(omega_);
}


// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //
void addNoiseR3(g2o::HyperGraph::Edge* edge_,
                GaussianSampler3& sampler_xyz_,
                const Matrix3& omega_) {
  EdgeSE3PointXYZ* e = dynamic_cast<EdgeSE3PointXYZ*>(edge_);
  if (!e) {
    throw std::runtime_error(exe_name+"|ERROR, invalid cast to EdgeSE3PointXYZ");
  }

  //ia estimate GT edge from current state (supposing that it is at the optimum)
  if (!e->setMeasurementFromState()) {
    throw std::runtime_error(exe_name+"|ERROR, cannot set measurement from state");
  }

  //ia add noise
  const Vector3 noise_xyz = sampler_xyz_.generateSample();
  const Vector3 noisy_meas = e->measurement() + noise_xyz;

  e->setMeasurement(noisy_meas);
  e->setInformation(omega_);  
}
