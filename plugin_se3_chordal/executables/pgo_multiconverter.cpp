#include <iostream>
#include <string>
#include <map>

//ia include fancy colors
#include "g2o/stuff/color_macros.h"

//ia include types
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include "g2o/types/slam3d/types_slam3d.h"
#include "g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h"

//ia include g2o core stuff
#include "g2o/stuff/command_args.h"
#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/factory.h"
#include "g2o/core/block_solver.h"
#include "g2o/solvers/cholmod/linear_solver_cholmod.h"
#include "g2o/solvers/csparse/linear_solver_csparse.h"
#include "g2o/core/optimization_algorithm.h"
#include "g2o/core/optimization_algorithm_gauss_newton.h"

//ia dl loader
#include "g2o/apps/g2o_cli/dl_wrapper.h"
#include "g2o/apps/g2o_cli/output_helper.h"
#include "g2o/apps/g2o_cli/g2o_common.h"

//ia include unscented
#include "g2o/plugin_se3_chordal/types_chordal3d/omega_converter_se3.h"

const std::string exe_name = "pgo_multiconverter|";
#define LOG std::cerr << exe_name


static constexpr int CONVERSION_ALL = 0; //-> generates all possible pgo factor graphs conversion\n";
static constexpr int CONVERSION_EULER_EULER  = 1; //-> generates Euler error +  Euler perturbation\n";
static constexpr int CONVERSION_QUAT_EULER   = 2; //-> generates Quaternion error +  Euler perturbation\n";
static constexpr int CONVERSION_CHORD_EULER  = 3; //-> generates Chordal error +  Euler perturbation\n";
static constexpr int CONVERSION_EULER_QUAT   = 4; //-> generates Euler error +  Quaternion perturbation\n";
static constexpr int CONVERSION_CHORD_QUAT   = 5; //-> generates Chordal error +  Quaternion perturbation\n";


using namespace g2o;

void deleteVertices(HyperGraph::VertexIDMap& vertices_);
void deleteEdges(HyperGraph::EdgeSet& edges_);
void writeVertices(const HyperGraph::VertexIDMap& v_map_,
                   std::ofstream& stream_);
void writeEdges(const HyperGraph::EdgeSet& e_set_,
                std::ofstream& stream_);

int main(int argc, char **argv) {
  LOG << "registering types...\n";
  // registering all the types from the libraries
  DlWrapper dl_types_wrapper;
  loadStandardTypes(dl_types_wrapper, argc, argv);
  LOG << CL_GREEN("done\n");

  // arguments of the executable
  std::string out_filename, input_filename;
  double chordal_omega_thresh = 1e-1;
  int conversion_type = -1;
  g2o::CommandArgs arg;
  arg.param("t", conversion_type, -1, "conversion type");
  arg.param("o", out_filename, "", "output filename - with no extension");
  arg.param("omegaThresh", chordal_omega_thresh, 1e-5, "threshold used to remap the information matrix of the edges chordal");
  arg.paramLeftOver("graph-input", input_filename, "", "graph file which will be processed - generated with g2o_simulator3d");
  arg.parseArgs(argc, argv);

  if (input_filename.empty()) {
    throw std::runtime_error("please specify an input file");
  }

  if (out_filename.empty()) {
    throw std::runtime_error("please specify output filename");
  }

  if (conversion_type < 0) {
    LOG << "invalid convesion type, choose from the following:" << std::endl;
    std::cerr << "\t[" << CL_YELLOW(CONVERSION_ALL) << "] -> generates all possible pgo factor graphs conversion\n";
    std::cerr << "\t[" << CL_YELLOW(CONVERSION_EULER_EULER) << "] -> generates Euler error +  Euler perturbation\n";
    std::cerr << "\t[" << CL_YELLOW(CONVERSION_QUAT_EULER) << "] -> generates Quaternion error +  Euler perturbation\n";
    std::cerr << "\t[" << CL_YELLOW(CONVERSION_CHORD_EULER) << "] -> generates Chordal error +  Euler perturbation\n";
    std::cerr << "\t[" << CL_YELLOW(CONVERSION_EULER_QUAT) << "] -> generates Euler error +  Quaternion perturbation\n";
    std::cerr << "\t[" << CL_YELLOW(CONVERSION_CHORD_QUAT) << "] -> generates Chordal error +  Quaternion perturbation\n";
    throw std::runtime_error(exe_name+"exit");
  }

  LOG << "loading graph [" << CL_YELLOW(input_filename) << "]" << std::endl;
  SparseOptimizer optimizer;
  optimizer.load(input_filename.c_str());

  auto& vertices_quaterion = optimizer.vertices();
  auto& src_edges    = optimizer.edges();

  const size_t& num_vertices = vertices_quaterion.size();
  const size_t& num_edges    = src_edges.size();

  size_t cnt = 0;

  LOG << "graph has "
      << CL_YELLOW(vertices_quaterion.size()) << " vertices and "
      << CL_YELLOW(src_edges.size()) << " egdes\n";


  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  LOG << CL_YELLOW("converting vertices: quaternion based -> euler based and angle axis based") << std::endl;
  g2o::SparseOptimizer::VertexIDMap vertices_euler;
  // g2o::SparseOptimizer::VertexIDMap vertices_angle_axis;
  for (const auto& v_tuple : vertices_quaterion) {
    float percentage = std::ceil((float)cnt++ / (float)num_vertices * 100);
    if ((int)percentage % 5 == 0)
      std::cerr << "\r" << exe_name << "completed " << percentage << "%" << std::flush;

    VertexSE3* v_quat = dynamic_cast<VertexSE3*>(v_tuple.second);
    if (!v_quat)
      throw std::runtime_error(exe_name+"input vertex should be a VertexSE3");

    //ia euler
    VertexSE3EulerPert* v_euler = new VertexSE3EulerPert();
    v_euler->setId(v_tuple.first);
    v_euler->setEstimate(v_quat->estimate());
    v_euler->setFixed(v_quat->fixed());
    vertices_euler.insert(std::make_pair(v_tuple.first, v_euler));

    //ia angle axis
//    VertexSE3AngleAxisPert* v_aa = new VertexSE3AngleAxisPert();
//    v_aa->setId(v_tuple.first);
//    v_aa->setEstimate(v_quat->estimate());
//    v_aa->setFixed(v_quat->fixed());
//    vertices_angle_axis.insert(std::make_pair(v_tuple.first, v_aa));
  }
  std::cerr << std::endl;


  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  LOG << CL_YELLOW("converting edges: geodesic-quaternion-error -> geodesic-euler-error") << std::endl;
  cnt = 0;

  g2o::SparseOptimizer::EdgeSet edges_geodesic_quaternion_euler;
  g2o::SparseOptimizer::EdgeSet edges_geodesic_euler_euler;
  g2o::SparseOptimizer::EdgeSet edges_geodesic_euler_quaternion;

  for (g2o::HyperGraph::Edge* e : src_edges) {
    float percentage = std::ceil((float)cnt++ / (float)num_edges * 100);
    if ((int)percentage % 5 == 0)
      std::cerr << "\r" << exe_name << "completed " << percentage << "%" << std::flush;

    EdgeSE3* e_quat = dynamic_cast<EdgeSE3*>(e);
    const auto& measurement = e_quat->measurement();
    const auto& omega = e_quat->information();
    const size_t& id_0 = e_quat->vertices()[0]->id();
    const size_t& id_1 = e_quat->vertices()[1]->id();

    EdgeSE3QuaternionErrorA* e_quat_euler = new EdgeSE3QuaternionErrorA();
    e_quat_euler->setMeasurement(measurement);
    e_quat_euler->setVertex(0, vertices_euler.at(id_0));
    e_quat_euler->setVertex(1, vertices_euler.at(id_1));
    e_quat_euler->setInformation(omega);
    edges_geodesic_quaternion_euler.insert(e_quat_euler);

    //ia compute new euler omega
//    LOG << CL_YELLOW("using the quaternion omega also for the euler") << std::endl;
    const Vector6 measurement_mean = internal::toVectorMQT(measurement);
    const Matrix6 omega_euler = omega;
//    const Matrix6 omega_euler = OmegaConverterSE3::convertOmegaQuaternion2Euler(measurement_mean, omega);

//    const Vector6 measurement_mean_euler = internal::toVectorET(measurement);
//    Matrix6 omega_recon =
//        OmegaConverterSE3::convertOmegaEuler2Quaternion(measurement_mean_euler, omega_euler);
//    LOG << "original = \n"
//        << CL_GREEN(omega) << std::endl;
//    LOG << "euler = \n"
//        << CL_YELLOW(omega_euler) << std::endl;
//    LOG << "recon = \n"
//        << CL_RED(omega_recon) << std::endl;
//    std::cin.get();

    EdgeSE3EulerErrorA* e_euler_euler = new EdgeSE3EulerErrorA();
    e_euler_euler->setMeasurement(measurement);
    e_euler_euler->setVertex(0, vertices_euler.at(id_0));
    e_euler_euler->setVertex(1, vertices_euler.at(id_1));
    e_euler_euler->setInformation(omega_euler);
    edges_geodesic_euler_euler.insert(e_euler_euler);

    EdgeSE3EulerErrorB* e_euler_quaternion = new EdgeSE3EulerErrorB();
    e_euler_quaternion->setMeasurement(measurement);
    e_euler_quaternion->setVertex(0, vertices_quaterion.at(id_0));
    e_euler_quaternion->setVertex(1, vertices_quaterion.at(id_1));
    e_euler_quaternion->setInformation(omega_euler);
    edges_geodesic_euler_quaternion.insert(e_euler_quaternion);
  }
  std::cerr << std::endl;


  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
/*
  g2o::SparseOptimizer::EdgeSet edges_angle_axis_euler;
  g2o::SparseOptimizer::EdgeSet edges_angle_axis_quaternion;
  g2o::SparseOptimizer::EdgeSet edges_angle_axis_angle_axis;
  LOG << CL_YELLOW("converting edges: geodesic-quaternion-error -> geodesic-angle_axis-error") << std::endl;
  cnt = 0;
  for (g2o::HyperGraph::Edge* e : src_edges) {
    float percentage = std::ceil((float)cnt++ / (float)num_edges * 100);
    if ((int)percentage % 5 == 0)
      std::cerr << "\r" << exe_name << "completed " << percentage << "%" << std::flush;

    EdgeSE3* e_quat = dynamic_cast<EdgeSE3*>(e);
    const auto& measurement = e_quat->measurement();
    const auto& omega = e_quat->information();
    const size_t& id_0 = e_quat->vertices()[0]->id();
    const size_t& id_1 = e_quat->vertices()[1]->id();

    const Vector6 measurement_mean = internal::toVectorMQT(measurement);
//    const Matrix6 omega_angle_axis = omega;
    const Matrix6 omega_angle_axis = OmegaConverterSE3::convertOmegaQuaternion2AngleAxis(measurement_mean, omega);

    EdgeSE3AngleAxisErrorA* e_aa_euler = new EdgeSE3AngleAxisErrorA();
    e_aa_euler->setMeasurement(measurement);
    e_aa_euler->setVertex(0, vertices_euler.at(id_0));
    e_aa_euler->setVertex(1, vertices_euler.at(id_1));
    e_aa_euler->setInformation(omega_angle_axis);
    edges_angle_axis_euler.insert(e_aa_euler);

    EdgeSE3AngleAxisErrorB* e_aa_quaternion = new EdgeSE3AngleAxisErrorB();
    e_aa_quaternion->setMeasurement(measurement);
    e_aa_quaternion->setVertex(0, vertices_quaterion.at(id_0));
    e_aa_quaternion->setVertex(1, vertices_quaterion.at(id_1));
    e_aa_quaternion->setInformation(omega_angle_axis);
    edges_angle_axis_quaternion.insert(e_aa_quaternion);

    EdgeSE3AngleAxisErrorC* e_aa_aa = new EdgeSE3AngleAxisErrorC();
    e_aa_aa->setMeasurement(measurement);
    e_aa_aa->setVertex(0, vertices_angle_axis.at(id_0));
    e_aa_aa->setVertex(1, vertices_angle_axis.at(id_1));
    e_aa_aa->setInformation(omega_angle_axis);
    edges_angle_axis_angle_axis.insert(e_aa_aa);
  }
/**/

  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  LOG << CL_YELLOW("converting edges: geodesic-quaternion-error -> chordal-error") << std::endl;
  cnt = 0;

  g2o::SparseOptimizer::EdgeSet edges_chordal_euler;
  g2o::SparseOptimizer::EdgeSet edges_chordal_quaternion;
  for (g2o::HyperGraph::Edge* e : src_edges) {
    float percentage = std::ceil((float)cnt++ / (float)num_edges * 100);
    if ((int)percentage % 5 == 0)
      std::cerr << "\r" << exe_name << "completed " << percentage << "%" << std::flush;

    EdgeSE3* e_quat = dynamic_cast<EdgeSE3*>(e);
    const auto& measurement = e_quat->measurement();
    const auto& omega = e_quat->information();
    const size_t& id_0 = e_quat->vertices()[0]->id();
    const size_t& id_1 = e_quat->vertices()[1]->id();

    //ia compute new euler omega
    const Vector6 measurement_mean = internal::toVectorMQT(measurement);
    Matrix12 omega_chordal =
        OmegaConverterSE3::convertOmegaQuaternion2Chordal(measurement_mean, omega, chordal_omega_thresh);


//    const Vector12 measurement_mean_chordal = internal::toFlatten(measurement);
//    Matrix6 omega_recon =
//        OmegaConverterSE3::convertOmegaChordal2Quaternion(measurement_mean_chordal, omega_chordal);
//    LOG << "original = \n"
//        << CL_GREEN(omega) << std::endl;
//    LOG << "chordal = \n"
//        << CL_YELLOW(omega_chordal) << std::endl;
//    LOG << "recon = \n"
//        << CL_RED(omega_recon) << std::endl;
//    std::cin.get();


    EdgeSE3ChordalErrorA* e_chordal_euler = new EdgeSE3ChordalErrorA();
    e_chordal_euler->setMeasurement(measurement);
    e_chordal_euler->setVertex(0, vertices_euler.at(id_0));
    e_chordal_euler->setVertex(1, vertices_euler.at(id_1));
    e_chordal_euler->setInformation(omega_chordal);
    edges_chordal_euler.insert(e_chordal_euler);

    EdgeSE3ChordalErrorB* e_chordal_quaternion = new EdgeSE3ChordalErrorB();
    e_chordal_quaternion->setMeasurement(measurement);
    e_chordal_quaternion->setVertex(0, vertices_quaterion.at(id_0));
    e_chordal_quaternion->setVertex(1, vertices_quaterion.at(id_1));
    e_chordal_quaternion->setInformation(omega_chordal);
    edges_chordal_quaternion.insert(e_chordal_quaternion);
  }
  std::cerr << std::endl;
  LOG << CL_GREEN("all conversions done\n");


  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  if (conversion_type == CONVERSION_ALL) {
    std::string graph_quaternion_euler = out_filename+"_quat_error_euler_pert.g2o";
    std::string graph_euler_euler = out_filename+"_euler_error_euler_pert.g2o";
    std::string graph_euler_quaternion = out_filename+"_euler_error_quat_pert.g2o";
    std::string graph_chordal_euler = out_filename+"_chord_error_euler_pert.g2o";
    std::string graph_chordal_quaternion = out_filename+"_chord_error_quat_pert.g2o";

    LOG << "writing new graphs\n"
        << "\t[" << CL_YELLOW(graph_quaternion_euler) << "]\n"
        << "\t[" << CL_YELLOW(graph_euler_euler) << "]\n"
        << "\t[" << CL_YELLOW(graph_euler_quaternion) << "]\n"
        << "\t[" << CL_YELLOW(graph_chordal_euler) << "]\n"
        << "\t[" << CL_YELLOW(graph_chordal_quaternion) << "]\n";

    std::ofstream stream_graph_quaternion_euler;
    std::ofstream stream_graph_euler_euler;
    std::ofstream stream_graph_euler_quaternion;
    std::ofstream stream_graph_chordal_euler;
    std::ofstream stream_graph_chordal_quaternion;

    stream_graph_quaternion_euler.open(graph_quaternion_euler.c_str());
    stream_graph_euler_euler.open(graph_euler_euler.c_str());
    stream_graph_euler_quaternion.open(graph_euler_quaternion.c_str());
    stream_graph_chordal_euler.open(graph_chordal_euler.c_str());
    stream_graph_chordal_quaternion.open(graph_chordal_quaternion.c_str());

    //ia writitng vertices
    writeVertices(vertices_euler, stream_graph_quaternion_euler);
    writeVertices(vertices_euler, stream_graph_euler_euler);
    writeVertices(vertices_euler, stream_graph_chordal_euler);
    writeVertices(vertices_quaterion, stream_graph_chordal_quaternion);
    writeVertices(vertices_quaterion, stream_graph_euler_quaternion);

    //ia writitng edges
    writeEdges(edges_geodesic_quaternion_euler, stream_graph_quaternion_euler);
    writeEdges(edges_geodesic_euler_euler, stream_graph_euler_euler);
    writeEdges(edges_geodesic_euler_quaternion, stream_graph_euler_quaternion);
    writeEdges(edges_chordal_euler, stream_graph_chordal_euler);
    writeEdges(edges_chordal_quaternion, stream_graph_chordal_quaternion);

    stream_graph_quaternion_euler.close();
    stream_graph_euler_euler.close();
    stream_graph_euler_quaternion.close();
    stream_graph_chordal_euler.close();
    stream_graph_chordal_quaternion.close();
  } else {
    HyperGraph::VertexIDMap* vertices_to_write = 0;
    HyperGraph::EdgeSet* edge_to_write = 0;
    switch (conversion_type) {
    case CONVERSION_EULER_EULER:
    {
      vertices_to_write = &vertices_euler;
      edge_to_write = &edges_geodesic_euler_euler;
      break;
    }

    case CONVERSION_QUAT_EULER:
    {
      vertices_to_write = &vertices_euler;
      edge_to_write = &edges_geodesic_quaternion_euler;
      break;
    }

    case CONVERSION_CHORD_EULER:
    {
      vertices_to_write = &vertices_euler;
      edge_to_write = &edges_chordal_euler;
      break;
    }

    case CONVERSION_EULER_QUAT:
    {
      vertices_to_write = &vertices_quaterion;
      edge_to_write = &edges_geodesic_euler_quaternion;
      break;
    }

    case CONVERSION_CHORD_QUAT:
    {
      vertices_to_write = &vertices_quaterion;
      edge_to_write = &edges_chordal_quaternion;
      break;
    }

    default:
      throw std::runtime_error(exe_name+"unknown conversion type");

    }

    std::string converted_file = out_filename;
    std::ofstream file;
    file.open(converted_file.c_str());
    writeVertices(*vertices_to_write, file);
    writeEdges(*edge_to_write, file);
    file.close();

  }

  LOG << CL_GREEN("done\n");
  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  // ----------------------------------------------------------------------- //
  //ia checkout things
  deleteVertices(vertices_euler);
  deleteEdges(edges_geodesic_euler_quaternion);
  deleteEdges(edges_geodesic_euler_euler);
  deleteEdges(edges_chordal_euler);
  deleteEdges(edges_chordal_quaternion);
  deleteEdges(edges_geodesic_quaternion_euler);

  return 0;
}




// ----------------------------------------------------------------------- //
// ----------------------------------------------------------------------- //
// ----------------------------------------------------------------------- //
// ----------------------------------------------------------------------- //
// ----------------------------------------------------------------------- //
// ----------------------------------------------------------------------- //
//ia monnezza
void writeVertices(const HyperGraph::VertexIDMap& v_map_,
                   std::ofstream& stream_) {
  for (const auto& v_tuple : v_map_) {
    g2o::OptimizableGraph::Vertex* v = dynamic_cast<g2o::OptimizableGraph::Vertex*>(v_tuple.second);
    const std::string vertex_tag = Factory::instance()->tag(v);
    stream_ << vertex_tag << " "
            << v_tuple.first << " ";
    v->write(stream_);
    stream_ << std::endl;

    //ia check for fixed vertices
    if (v->fixed()) {
      stream_ << "FIX " << v_tuple.first << std::endl;
    }
  }
}

void writeEdges(const HyperGraph::EdgeSet& e_set_,
                std::ofstream& stream_) {
  for (HyperGraph::Edge* e : e_set_) {
    g2o::OptimizableGraph::Edge* edge = dynamic_cast<g2o::OptimizableGraph::Edge*>(e);
    stream_ << Factory::instance()->tag(edge) << " "
            << edge->vertex(0)->id() << " "
            << edge->vertex(1)->id() << " ";
    edge->write(stream_);
    stream_ << std::endl;
  }
}

void deleteVertices(HyperGraph::VertexIDMap& vertices_) {
  for (auto& v_tuple : vertices_) {
    if (v_tuple.second) {
      delete v_tuple.second;
      v_tuple.second = nullptr;
    }
  }
}

void deleteEdges(HyperGraph::EdgeSet& edges_) {
  for (HyperGraph::Edge* e : edges_) {
    delete e;
  }
}










