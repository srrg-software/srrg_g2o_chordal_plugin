#include <iostream>
#include <string>
#include <map>

//ia include fancy colors
#include "g2o/stuff/color_macros.h"

//ia include types
#include "g2o/types/slam3d/types_slam3d.h"
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include "g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h"
#include "g2o/plugin_se3_chordal/types_chordal_initialization3d/types_chordal_initialization3d.h"
#include "g2o/plugin_se3_chordal/types_chordal_initialization3d/chordal_initializer_se3.h"

//ia include g2o core stuff
#include "g2o/stuff/command_args.h"
#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/factory.h"
#include "g2o/core/block_solver.h"
#include "g2o/solvers/cholmod/linear_solver_cholmod.h"
#include "g2o/solvers/csparse/linear_solver_csparse.h"
#include "g2o/core/optimization_algorithm.h"
#include "g2o/core/optimization_algorithm_gauss_newton.h"

////ia include unscented
//#include "g2o/stuff/unscented.h"

#include <Eigen/Core>
#include <Eigen/StdVector>

#define LOG std::cerr << "test_chordal_initialization|"

using namespace g2o;

using InitilizationBlockSolver = g2o::BlockSolver<g2o::BlockSolverTraits<-1, -1>>;
using InitilizationLinearSolverCholmod = g2o::LinearSolverCholmod<InitilizationBlockSolver::PoseMatrixType>;
using InitilizationLinearSolverCSparse = g2o::LinearSolverCSparse<InitilizationBlockSolver::PoseMatrixType>;

int main(int argc, char **argv) {
  //ia factory setup and tags
  EdgeSE3ChordalErrorA* e_factory_0 = new EdgeSE3ChordalErrorA();
  EdgeSE3ChordalInitRotation* e_factory_1 = new EdgeSE3ChordalInitRotation();
  EdgeSE3ChordalInitTranslation* e_factory_2 = new EdgeSE3ChordalInitTranslation();
  delete e_factory_0; e_factory_0 = nullptr;
  delete e_factory_1; e_factory_1 = nullptr;
  delete e_factory_2; e_factory_2 = nullptr;

  // arguments of the executable
  std::string out_filename, input_filename;
  g2o::CommandArgs arg;
  arg.param("o", out_filename, "", "output of the initialization");
  arg.paramLeftOver("graph-input", input_filename, "", "graph file which will be processed [chordal]");
  arg.parseArgs(argc, argv);

  if (input_filename.empty()) {
    throw std::runtime_error("please specify an in file");
  }
  if (out_filename.empty()) {
    throw std::runtime_error("please specify an out file");
  }


  // loading the graph
  SparseOptimizer src_graph;
  std::unique_ptr<InitilizationLinearSolverCholmod> linear_solver = g2o::make_unique<InitilizationLinearSolverCholmod>();
  linear_solver->setBlockOrdering(true);
  std::unique_ptr<InitilizationBlockSolver> block_solver = g2o::make_unique<InitilizationBlockSolver>(std::move(linear_solver));
  g2o::OptimizationAlgorithmGaussNewton* solver = new g2o::OptimizationAlgorithmGaussNewton(std::move(block_solver));
  if (!solver) throw std::runtime_error("impossible to create optimization algoritm");
  src_graph.setAlgorithm(solver);

  LOG << "opening the file : " << input_filename << std::endl;
  std::ifstream ifs(input_filename.c_str());
  src_graph.load(ifs);

  LOG << "loaded " << CL_YELLOW(src_graph.vertices().size()) << " vertexes" << std::endl;
  LOG << "loaded " << CL_YELLOW(src_graph.edges().size()) << " edges" << std::endl;

  //ia compute the initial error using chordal error function
  src_graph.initializeOptimization();
  src_graph.computeActiveErrors();
  LOG << "source chi2 [pre initialization]= " << CL_YELLOW(src_graph.activeChi2()) << std::endl;

  //ia compute initialization
  ChordalInitializerSE3::initializeGraph(src_graph);

  src_graph.computeActiveErrors();
  LOG << "source chi2 = [post initialization] = " << CL_GREEN(src_graph.activeChi2()) << std::endl;

  LOG << "saving output in " << CL_YELLOW(out_filename) << std::endl;
  src_graph.save(out_filename.c_str());

  return 0;
}


