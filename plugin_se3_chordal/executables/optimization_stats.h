#pragma once
#include "g2o/stuff/color_macros.h"
#include "g2o/stuff/macros.h"
#include <algorithm>
#include <cassert>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

namespace g2o {

  struct OptimizationStats {
    const int iteration;
    const double chi2;
    const double chi2_k;
    const double it_time;
    const size_t nnz;
    const number_t delta_chi2;
    const number_t perturbation_norm;

    OptimizationStats() :
      iteration(0),
      chi2(-1.0f),
      chi2_k(-1.0f),
      it_time(0),
      nnz(0),
      delta_chi2(0.0),
      perturbation_norm(0.0) {
    }

    virtual ~OptimizationStats() {
    }

    OptimizationStats(const int& it_,
                      const double& chi2_,
                      const double& chi2_k_,
                      const double& time_,
                      const size_t& nnz_,
                      const number_t& delta_chi2_        = 0.0,
                      const number_t& perturbation_norm_ = 0.0) :
      iteration(it_),
      chi2(chi2_),
      chi2_k(chi2_k_),
      it_time(time_),
      nnz(nnz_),
      delta_chi2(delta_chi2_),
      perturbation_norm(perturbation_norm_) {
    }
  };

  std::ostream& operator<<(std::ostream& os_, const OptimizationStats& stats_) {
    os_ << "it= " << CL_BLUE(FIXED(stats_.iteration)) << "\t "
        << "chi2= " << FIXED(stats_.chi2) << "\t "
        << "chi2Kernelized= " << FIXED(stats_.chi2_k) << "\t "
        << "itTime= " << FIXED(stats_.it_time) << "\t "
        << "cholNNZ= " << FIXED(stats_.nnz) << "\t "
        << "deltaChi2= " << FIXED(stats_.delta_chi2) << "; "
        << "pertNorm= " << FIXED(stats_.perturbation_norm) << "; ";
    return os_;
  }

  std::ofstream& operator<<(std::ofstream& ofs_,
                            const OptimizationStats& stats_) {
    ofs_ << "it= " << FIXED(stats_.iteration) << "; "
         << "chi2= " << FIXED(stats_.chi2) << "; "
         << "chi2Kernelized= " << FIXED(stats_.chi2_k) << "; "
         << "itTime= " << FIXED(stats_.it_time) << "; "
         << "cholNNZ= " << FIXED(stats_.nnz) << "; "
         << "deltaChi2= " << FIXED(stats_.delta_chi2) << "; "
         << "pertNorm= " << FIXED(stats_.perturbation_norm) << "; ";
    return ofs_;
  }

  typedef std::vector<OptimizationStats> OptimizationStatsVector;

  struct OptimizationComparisonStats : public OptimizationStats {
    const double reproj_chi2;
    const double reproj_chi2_k;

    OptimizationComparisonStats() :
      OptimizationStats(),
      reproj_chi2(0.0f),
      reproj_chi2_k(0.0f) {
    }

    virtual ~OptimizationComparisonStats() {
    }

    OptimizationComparisonStats(const int& it_,
                                const double& chi2_,
                                const double& chi2_k_,
                                const double& time_,
                                const size_t& nnz_,
                                const double& reproj_chi2_,
                                const double& reproj_chi2_k_,
                                const number_t& delta_chi2_        = 0.0,
                                const number_t& perturbation_norm_ = 0.0) :
      OptimizationStats(it_,
                        chi2_,
                        chi2_k_,
                        time_,
                        nnz_,
                        delta_chi2_,
                        perturbation_norm_),
      reproj_chi2(reproj_chi2_),
      reproj_chi2_k(reproj_chi2_k_) {
    }
  };

  typedef std::vector<OptimizationComparisonStats>
    OptimizationComparisonStatsVector;
  std::ostream& operator<<(std::ostream& os_,
                           const OptimizationComparisonStats& stats_) {
    os_ << "it= " << CL_BLUE(FIXED(stats_.iteration)) << "\t "
        << "chi2= " << FIXED(stats_.chi2) << "\t "
        << "chi2K= " << FIXED(stats_.chi2_k) << "\t "
        << "itTime= " << FIXED(stats_.it_time) << "\t "
        << "cholNNZ= " << FIXED(stats_.nnz) << "\t "
        << "reprojChi2= " << FIXED(stats_.reproj_chi2) << "\t "
        << "reprojChi2K= " << FIXED(stats_.reproj_chi2_k) << "\t "
        << "deltaChi2= " << FIXED(stats_.delta_chi2) << "; "
        << "pertNorm= " << FIXED(stats_.perturbation_norm) << "; ";
    return os_;
  }

  std::ofstream& operator<<(std::ofstream& ofs_,
                            const OptimizationComparisonStats& stats_) {
    ofs_ << "it= " << FIXED(stats_.iteration) << "; "
         << "chi2= " << FIXED(stats_.chi2) << "; "
         << "chi2K= " << FIXED(stats_.chi2_k) << "; "
         << "itTime= " << FIXED(stats_.it_time) << "; "
         << "cholNNZ= " << FIXED(stats_.nnz) << "; "
         << "reprojChi2= " << FIXED(stats_.reproj_chi2) << "; "
         << "reprojChi2K= " << FIXED(stats_.reproj_chi2_k) << "; "
         << "deltaChi2= " << FIXED(stats_.delta_chi2) << "; "
         << "pertNorm= " << FIXED(stats_.perturbation_norm) << "; ";
    return ofs_;
  }

} // namespace g2o
