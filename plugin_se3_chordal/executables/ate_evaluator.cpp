#include <algorithm>
#include <cassert>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <signal.h>
#include <sstream>
#include <string>

#include "g2o/apps/g2o_cli/dl_wrapper.h"
#include "g2o/apps/g2o_cli/g2o_common.h"
#include "g2o/apps/g2o_cli/output_helper.h"

#include "g2o/config.h"
#include "g2o/core/batch_stats.h"
#include "g2o/core/estimate_propagator.h"
#include "g2o/core/factory.h"
#include "g2o/core/hyper_dijkstra.h"
#include "g2o/core/hyper_graph_action.h"
#include "g2o/core/optimization_algorithm_factory.h"
#include "g2o/core/sparse_optimizer.h"

#include "g2o/core/optimization_algorithm.h"
#include "g2o/core/robust_kernel.h"
#include "g2o/core/robust_kernel_factory.h"
#include "g2o/core/sparse_optimizer_terminate_action.h"

#include "g2o/stuff/color_macros.h"
#include "g2o/stuff/command_args.h"
#include "g2o/stuff/filesys_tools.h"
#include "g2o/stuff/macros.h"
#include "g2o/stuff/string_tools.h"
#include "g2o/stuff/timeutil.h"

#include "g2o/plugin_se3_chordal/types_chordal3d/types_chordal3d.h"
#include "g2o/plugin_se3_chordal/types_chordal_initialization3d/types_chordal_initialization3d.h"
#include "g2o/types/slam3d/types_slam3d.h"
#include "g2o/types/slam2d/types_slam2d.h"

// ia include unscented
#include "g2o/stuff/unscented.h"

// ia include chordal initializer
#include "g2o/plugin_se3_chordal/types_chordal_initialization3d/chordal_initializer_se3.h"
#include "optimization_stats.h"

using namespace g2o;

const std::string exe_name("ate_evaluator");
#define LOG std::cerr << exe_name + "|"

//ia 3d ate (templetized to deal with different types of 3d pose edges)
template <typename GT_VType, typename Query_VType>
void evaluateAte(const g2o::SparseOptimizer& gt_graph_,
                 const g2o::SparseOptimizer& input_graph_,
                 double& ate_translational_,
                 double& ate_rotational_);

//ia 2d ate, simple
void evaluateAte2D(const g2o::SparseOptimizer& gt_graph_,
                   const g2o::SparseOptimizer& input_graph_,
                   double& ate_translational_,
                   double& ate_rotational_);

int main(int argc, char** argv) {
  //ia registering all the types from the libraries
  g2o::DlWrapper dlTypesWrapper;
  loadStandardTypes(dlTypesWrapper, argc, argv);
  //ia register all the solvers
  g2o::DlWrapper dlSolverWrapper;
  loadStandardSolver(dlSolverWrapper, argc, argv);

  //ia shell arguments
  std::string gt_filename = "";
  std::string input_filename = "";
  bool input_is_chordal = false;
  bool input_is_2d = false;
  
  g2o::CommandArgs arg;
  arg.param("gt",
            gt_filename,
            "",
            "gt graph - should be VERTEXSE3:QUAT for 3D or VERTEX_SE2 for 2D");
  arg.param("i",
            input_filename,
            "",
            "input filename");
  arg.param("bidimensional",
            input_is_2d,
            false,
            "true if input graph is 2D, aka VERTEX_SE2");
  arg.param("chordal",
            input_is_chordal,
            false,
            "true if input graph is VERTEX_SE3:EULERPERT");
  arg.parseArgs(argc, argv);

  if (gt_filename == "" || input_filename == "") {
    throw std::runtime_error(exe_name+"|specify a file for the gt and the input file");
  }

  //ia load gt
  g2o::SparseOptimizer gt_graph;
  gt_graph.setVerbose(false);
  LOG << "opening GT file [" << CL_YELLOW(gt_filename) << "]\n";
  std::ifstream gt_stream(gt_filename.c_str());
  if (!gt_stream) {
    throw std::runtime_error(exe_name+"|failed to open the selected file, exit");
  }
  gt_graph.load(gt_stream);
  const size_t gt_num_v = gt_graph.vertices().size();
  const size_t gt_num_e = gt_graph.edges().size();
  LOG << "GT graph has [" << gt_num_v
      << "] vertices and [" << gt_num_e << "] edges" << std::endl;

  //ia load query graph
  g2o::SparseOptimizer input_graph;
  input_graph.setVerbose(false);
  LOG << "opening INPUT file [" << CL_YELLOW(input_filename) << "]\n";
  std::ifstream input_stream(input_filename.c_str());
  if (!input_stream) {
    throw std::runtime_error(exe_name+"|failed to open the selected file, exit");
  }
  input_graph.load(input_stream);
  const size_t input_num_v = input_graph.vertices().size();
  const size_t input_num_e = input_graph.edges().size();
  LOG << "INPUT graph has [" << input_num_v
      << "] vertices and [" << input_num_e << "] edges" << std::endl;

  //ia do the magic trick
  double ate_t = 0.f;
  double ate_r = 0.f;
  
  if (!input_is_2d) {
    LOG << "evaluating 3D ATE\n";
    if (input_is_chordal) {
      evaluateAte<g2o::VertexSE3,g2o::VertexSE3EulerPert>(gt_graph, input_graph, ate_t, ate_r);
    } else {
      evaluateAte<g2o::VertexSE3,g2o::VertexSE3>(gt_graph, input_graph, ate_t, ate_r);
    }
  } else {
    LOG << "evaluating 2D ATE\n";
    evaluateAte2D(gt_graph, input_graph, ate_t, ate_r);
  }

  return 0;
}

template <typename GT_VType, typename Query_VType>
void evaluateAte(const g2o::SparseOptimizer& gt_graph_,
                 const g2o::SparseOptimizer& query_graph_,
                 double& ate_translational_,
                 double& ate_rotational_) {
  ate_translational_ = 0.f;
  ate_rotational_ = 0.f;

  
  const auto& gt_vertices = gt_graph_.vertices();
  const auto& query_vertices = query_graph_.vertices();
  // const float inverse_num_vertices = 1.f/(float)(gt_vertices.size());

  if (gt_vertices.size() != query_vertices.size()) {
    throw std::runtime_error(exe_name+"|mismatch");
  }

  size_t skipped = 0;
  size_t processed = 0;

  for (const auto& gt_id_v_pair : gt_vertices) {
    GT_VType* gt_v = dynamic_cast<GT_VType*>(gt_id_v_pair.second);
    Query_VType* query_v = dynamic_cast<Query_VType*>(query_vertices.at(gt_id_v_pair.first));

    if (gt_v == nullptr) {
      ++skipped;
      continue;
    }
    
    if (query_v == nullptr) {
      ++skipped;
      continue;
    }

    const Isometry3 delta = gt_v->estimate().inverse() * query_v->estimate();
    // const Vector6 error = internal::toVectorMQT(delta);
    const Vector6 error = internal::toVectorET(delta); //ia use euler angles
    const Vector3 error_trans = error.head(3);
    const Vector3 error_rot = error.tail(3);

    // std::cerr << "error = " << error.transpose() << std::endl;
    // std::cerr << "error trans = " << error_trans.transpose() << std::endl;
    // std::cerr << "error rot = " << error_rot.transpose() << std::endl;

    // LOG << "vertex [" << gt_id_v_pair.first
    //     << "] translational error norm = " << error_trans.norm() << std::endl;

    // LOG << "vertex [" << gt_id_v_pair.first
    //     << "] rotational error norm = " << error_rot.norm()
    //     << " [ " << error_rot.transpose() << " ] "
    //     << std::endl;

    if (std::isnan(error_trans.norm()) || std::isnan(error_rot.norm())) {
      LOG << "WARNING, detected nan, skipping vertex [ " << gt_id_v_pair.first << " ]\n";
      ++skipped;
      continue;
    }

    //ia this should not be the norm but the squared error
    // ate_translational_ += error_trans.norm();
    // ate_rotational_ += error_rot.norm();
    ate_translational_ += error_trans.transpose() * error_trans;
    ate_rotational_ += error_rot.transpose() * error_rot;
    ++processed;
  }

  //ia finished the raw error accumulation, check if we actually have data
  LOG << "processed vertices [ " << processed << "/" << gt_vertices.size() << " ]\n";
  LOG << "skipped vertices [ " << skipped  << "/" << gt_vertices.size() << " ]\n";
  if (!processed) {
    throw std::runtime_error(exe_name+"|invalid types");
  }

  // std::cerr << "raw trans = " << ate_translational << std::endl;
  // std::cerr << "raw rot = " << ate_rotational << std::endl;
  const float inverse_num_vertices = 1.f/(float)(processed);

  //ia compute ate
  ate_translational_ *= inverse_num_vertices;
  ate_rotational_ *= inverse_num_vertices;

  ate_translational_ = std::sqrt(ate_translational_);
  ate_rotational_ = std::sqrt(ate_rotational_);
  
  LOG << "translational ate = " << CL_GREEN(ate_translational_) << std::endl;
  LOG << "rotational ate = " << CL_GREEN(ate_rotational_) << std::endl;

  std::cout << std::fixed;
  std::cout << std::setprecision(3);
  std::cout << "t_ate= " << ate_translational_
            << "; r_ate= " << ate_rotational_ << "; \n";
}


//ia 2d world
void evaluateAte2D(const g2o::SparseOptimizer& gt_graph_,
                   const g2o::SparseOptimizer& query_graph_,
                   double& ate_translational_,
                   double& ate_rotational_) {
  ate_translational_ = 0.f;
  ate_rotational_ = 0.f;

  
  const auto& gt_vertices = gt_graph_.vertices();
  const auto& query_vertices = query_graph_.vertices();
  // const float inverse_num_vertices = 1.f/(float)(gt_vertices.size());

  if (gt_vertices.size() != query_vertices.size()) {
    throw std::runtime_error(exe_name+"|mismatch");
  }

  size_t skipped = 0;
  size_t processed = 0;

  for (const auto& gt_id_v_pair : gt_vertices) {
    VertexSE2* gt_v = dynamic_cast<VertexSE2*>(gt_id_v_pair.second);
    VertexSE2* query_v = dynamic_cast<VertexSE2*>(query_vertices.at(gt_id_v_pair.first));

    if (gt_v == nullptr) {
      ++skipped;
      continue;
    }
    
    if (query_v == nullptr) {
      ++skipped;
      continue;
    }

    ++processed;

    //ia BLEARGH g2o uses custom SE2 type :(((((((
    const SE2 delta = gt_v->estimate().inverse() * query_v->estimate();
    const Vector3 error = delta.toVector();

    const Vector2 error_trans = error.head(2);
    const number_t error_rot  = error[2];

    // std::cerr << "error = " << error.transpose() << std::endl;
    // std::cerr << "error trans = " << error_trans.transpose() << std::endl;
    // std::cerr << "error rot = " << error_rot.transpose() << std::endl;

    // LOG << "vertex [" << gt_id_v_pair.first
    //     << "] translational error norm = " << error_trans.norm() << std::endl;

    ate_translational_ += error_trans.transpose() * error_trans;
    ate_rotational_ += error_rot * error_rot;
  }

  //ia finished the raw error accumulation, check if we actually have data
  LOG << "processed vertices [ " << processed << "/" << gt_vertices.size() << " ]\n";
  LOG << "skipped vertices [ " << skipped  << "/" << gt_vertices.size() << " ]\n";
  if (!processed) {
    throw std::runtime_error(exe_name+"|invalid types");
  }

  // std::cerr << "raw trans = " << ate_translational_ << std::endl;
  // std::cerr << "raw rot = " << ate_rotational_ << std::endl;
  const float inverse_num_vertices = 1.f/(float)(processed);

  //ia compute ate
  ate_translational_ *= inverse_num_vertices;
  ate_rotational_ *= inverse_num_vertices;

  ate_translational_ = std::sqrt(ate_translational_);
  ate_rotational_ = std::sqrt(ate_rotational_);
  
  LOG << "translational ate = " << CL_GREEN(ate_translational_) << std::endl;
  LOG << "rotational ate = " << CL_GREEN(ate_rotational_) << std::endl;

  std::cout << std::fixed;
  std::cout << std::setprecision(3);
  std::cout << "t_ate= " << ate_translational_
            << "; r_ate= " << ate_rotational_ << "; \n";
}
