#pragma once
#include "g2o/config.h"
#include "g2o/core/base_vertex.h"
#include "g2o/core/hyper_graph_action.h"
#include "g2o/types/slam3d/isometry3d_mappings.h"
#include "g2o/types/slam3d/g2o_types_slam3d_api.h"
#include "chordal_mappings.h"

// IA THIS DOESN'T WORK

namespace g2o {

  /**
   * \brief 3D pose Vertex, represented as an Isometry3
   *
   * 3D pose vertex, represented as an Isometry3, i.e., an affine transformation
   * which is constructed by only concatenating rotation and translation
   * matrices. Hence, no scaling or projection.  To avoid that the rotational
   * part of the Isometry3 gets numerically unstable we compute the nearest
   * orthogonal matrix after a large number of calls to the oplus method.
   *
   * The parameterization for the increments constructed is a 6d vector
   * (x,y,z,saa1,saa2,saa3) (translation and scaled_axis_angle).
   * The scaled_axis_angle represents the Euler axis scaled by its angle (not unitary)
   */
  class VertexSE3AngleAxisPert : public BaseVertex<6,Isometry3> {
  public:
    static const int orthogonalizeAfter = 1000; //< orthogonalize the rotation matrix after N updates

    VertexSE3AngleAxisPert();

    virtual void setToOriginImpl() {
      _estimate = Isometry3::Identity();
    }

    virtual bool read(std::istream& is);
    virtual bool write(std::ostream& os) const;

    virtual bool setEstimateDataImpl(const number_t* est){
      Eigen::Map<const Vector7> v(est);
      _estimate=internal::fromVectorQT(v);
      return true;
    }

    virtual bool getEstimateData(number_t* est) const{
      Eigen::Map<Vector7> v(est);
      v=internal::toVectorQT(_estimate);
      return true;
    }

    virtual int estimateDimension() const {
      return 7;
    }

    virtual bool setMinimalEstimateDataImpl(const number_t* est){
      Eigen::Map<const Vector6> v(est);
      _estimate = internal::fromVectorSAA(v);
      return true;
    }

    virtual bool getMinimalEstimateData(number_t* est) const{
      Eigen::Map<Vector6> v(est);
      v = internal::toVectorSAA(_estimate);
      return true;
    }

    virtual int minimalEstimateDimension() const {
      return 6;
    }

    /**
     * update the position of this vertex. The update is in the form
     * (x,y,z,saa1,saa2,saa3) whereas (x,y,z) represents the translational update
     * and (saa1,saa2,saa3) corresponds to the components of the Euler axis scaled by the angle.
     */
    virtual void oplusImpl(const number_t* update) {
      Eigen::Map<const Vector6> v(update);
//      std::cerr << "increment = " << v.transpose() << std::endl;
      Isometry3 increment = internal::fromVectorSAA(v);
      _estimate = increment * _estimate;
      if (++_num_oplus_calls > orthogonalizeAfter) {
        _num_oplus_calls = 0;
        internal::approximateNearestOrthogonalMatrix(_estimate.matrix().topLeftCorner<3,3>());
      }
    }

    //! wrapper function to use the old SE3 type
    SE3Quat G2O_ATTRIBUTE_DEPRECATED(estimateAsSE3Quat() const) { return internal::toSE3Quat(estimate());}
    //! wrapper function to use the old SE3 type
    void G2O_ATTRIBUTE_DEPRECATED(setEstimateFromSE3Quat(const SE3Quat& se3)) { setEstimate(internal::fromSE3Quat(se3));}

  protected:
    ///< store how often opluse was called to trigger orthogonaliation of the rotation matrix
    int _num_oplus_calls;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };




#ifdef G2O_HAVE_OPENGL
  /**
   * \brief visualize the 3D pose vertex
   */
  class G2O_TYPES_SLAM3D_API VertexSE3AngleAxisPertDrawAction: public DrawAction{
  public:
    VertexSE3AngleAxisPertDrawAction();
    virtual HyperGraphElementAction* operator()(HyperGraph::HyperGraphElement* element, HyperGraphElementAction::Parameters* params_);
  protected:
    virtual bool refreshPropertyPtrs(HyperGraphElementAction::Parameters* params_);
    FloatProperty* _triangleX, *_triangleY;
  };
#endif

} /* namespace g2o */

