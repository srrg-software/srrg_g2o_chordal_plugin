#include "edge_se3_quaternion_error_a.h"
#include "g2o/types/slam3d/isometry3d_gradients.h"

#ifdef G2O_HAVE_OPENGL
#include "g2o/stuff/opengl_wrapper.h"
#include "g2o/stuff/opengl_primitives.h"
#endif

namespace g2o {

  EdgeSE3QuaternionErrorA::EdgeSE3QuaternionErrorA() :
    BaseBinaryEdge<6, Isometry3, VertexSE3EulerPert, VertexSE3EulerPert>() {
    _inverse_measurement.setIdentity();
    _information.setIdentity();
  }


  bool EdgeSE3QuaternionErrorA::read(std::istream& is_) {
    //ia measurement
    Vector7 meas = Vector7::Zero();
    for (uint16_t i = 0; i < 7; i++)
      is_ >> meas[i];

    // normalize the quaternion to recover numerical precision lost by storing as human readable text
    Vector4::MapType(meas.data()+3).normalize();
    setMeasurement(internal::fromVectorQT(meas));
    if (is_.bad()) {
      return false;
    }

    //ia information matrix
    for ( int i=0; i<information().rows() && is_.good(); i++)
      for (int j=i; j<information().cols() && is_.good(); j++){
        is_ >> information()(i,j);
        if (i!=j)
          information()(j,i)=information()(i,j);
      }
    if (is_.bad()) {
      //  we overwrite the information matrix with the Identity
      information().setIdentity();
    }
    return true;
  }

  bool EdgeSE3QuaternionErrorA::write(std::ostream& os_) const {
    Vector7 meas=internal::toVectorQT(_measurement);
    for (uint16_t i = 0; i < 7; i++)
      os_ << meas[i] << " ";
    for (uint16_t i = 0; i < information().rows(); i++)
      for (uint16_t j = i; j < information().cols(); j++) {
        os_ <<  information()(i,j) << " ";
      }
    return os_.good();
  }

  bool EdgeSE3QuaternionErrorA::setMeasurementFromState() {
    VertexSE3EulerPert* from = static_cast<VertexSE3EulerPert*>(_vertices[0]);
    VertexSE3EulerPert* to   = static_cast<VertexSE3EulerPert*>(_vertices[1]);
    Isometry3 delta = from->estimate().inverse() * to->estimate();
    setMeasurement(delta);
    return true;
  }

  void EdgeSE3QuaternionErrorA::initialEstimate(const OptimizableGraph::VertexSet& from_,
                                                OptimizableGraph::Vertex* /*to_*/) {
    VertexSE3EulerPert* from = static_cast<VertexSE3EulerPert*>(_vertices[0]);
    VertexSE3EulerPert* to = static_cast<VertexSE3EulerPert*>(_vertices[1]);
    if (from_.count(from) > 0) {
      to->setEstimate(from->estimate() * _measurement);
    } else {
      from->setEstimate(to->estimate() * _inverse_measurement);
    }
  }

  void EdgeSE3QuaternionErrorA::computeError() {
    VertexSE3EulerPert* from = dynamic_cast<VertexSE3EulerPert*>(_vertices[0]);
    VertexSE3EulerPert* to   = dynamic_cast<VertexSE3EulerPert*>(_vertices[1]);
    Isometry3 delta = _inverse_measurement * from->estimate().inverse() * to->estimate();
    _error = internal::toVectorMQT(delta);
  }

//  void EdgeSE3QuaternionErrorA::linearizeOplus(){
//
//    // BaseBinaryEdge<6, Isometry3, VertexSE3, VertexSE3>::linearizeOplus();
//    // return;
//
//    VertexSE3EulerPert* from = static_cast<VertexSE3EulerPert*>(_vertices[0]);
//    VertexSE3EulerPert* to   = static_cast<VertexSE3EulerPert*>(_vertices[1]);
//    Isometry3 E = Isometry3::Identity();
//    const Isometry3& Xi=from->estimate();
//    const Isometry3& Xj=to->estimate();
//    const Isometry3& Z=_measurement;
//    internal::computeEdgeSE3Gradient(E, _jacobianOplusXi , _jacobianOplusXj, Z, Xi, Xj);
//  }


#ifdef G2O_HAVE_OPENGL
  EdgeSE3QuaternionErrorADrawAction::EdgeSE3QuaternionErrorADrawAction() : DrawAction(typeid(EdgeSE3QuaternionErrorA).name()) {
    //ia parent ctor
  }

  HyperGraphElementAction* EdgeSE3QuaternionErrorADrawAction::operator()(HyperGraph::HyperGraphElement* element,
                                                                         HyperGraphElementAction::Parameters* params) {
    if (typeid(*element).name()!=_typeName)
      return 0;
    refreshPropertyPtrs(params);
    if (! _previousParams)
      return this;

    if (_show && !_show->value())
      return this;

    EdgeSE3QuaternionErrorA* e =  static_cast<EdgeSE3QuaternionErrorA*>(element);
    VertexSE3EulerPert* fromEdge = static_cast<VertexSE3EulerPert*>(e->vertices()[0]);
    VertexSE3EulerPert* toEdge   = static_cast<VertexSE3EulerPert*>(e->vertices()[1]);
    if (! fromEdge || ! toEdge)
      return this;
    glColor3f(POSE_EDGE_COLOR);
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glBegin(GL_LINES);
    glVertex3f((float)fromEdge->estimate().translation().x(),(float)fromEdge->estimate().translation().y(),(float)fromEdge->estimate().translation().z());
    glVertex3f((float)toEdge->estimate().translation().x(),(float)toEdge->estimate().translation().y(),(float)toEdge->estimate().translation().z());
    glEnd();
    glPopAttrib();
    return this;
  }
#endif

} /* namespace g2o */
