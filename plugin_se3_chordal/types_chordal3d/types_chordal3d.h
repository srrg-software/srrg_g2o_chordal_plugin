#pragma once

#include "g2o/config.h"
#include "g2o/core/base_vertex.h"
#include "g2o/core/base_binary_edge.h"
#include "g2o/core/hyper_graph_action.h"

//! @brief euler perturbation
#include "vertex_se3_euler_pert.h"
#include "edge_se3_quaternion_error_a.h"
#include "edge_se3_chordal_error_a.h"
#include "edge_se3_euler_error_a.h"

//! @brief quaternion perturbation
#include "edge_se3_chordal_error_b.h"
#include "edge_se3_euler_error_b.h"

//! @brief angle axis perturbation
#include "vertex_se3_angle_axis_pert.h"
#include "edge_se3_angle_axis_error_a.h"
#include "edge_se3_angle_axis_error_b.h"
#include "edge_se3_angle_axis_error_c.h"

