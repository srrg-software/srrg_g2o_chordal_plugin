#include "edge_se3_angle_axis_error_c.h"
#ifdef G2O_HAVE_OPENGL
#include "g2o/stuff/opengl_wrapper.h"
#include "g2o/stuff/opengl_primitives.h"
#endif

namespace g2o {

  EdgeSE3AngleAxisErrorC::EdgeSE3AngleAxisErrorC():
          BaseBinaryEdge<6, Isometry3, VertexSE3AngleAxisPert, VertexSE3AngleAxisPert>() {
    _inverse_measurement.setIdentity();
    _information.setIdentity();
  }


  bool EdgeSE3AngleAxisErrorC::read(std::istream& is_) {
    //ia measurement
    Vector7 meas = Vector7::Zero();
    for (uint16_t i = 0; i < 7; i++)
      is_ >> meas[i];

    // normalize the quaternion to recover numerical precision lost by storing as human readable text
    Vector4::MapType(meas.data()+3).normalize();
    setMeasurement(internal::fromVectorQT(meas));
    if (is_.bad()) {
      return false;
    }

    //ia information matrix
    for ( int i=0; i<information().rows() && is_.good(); i++)
      for (int j=i; j<information().cols() && is_.good(); j++){
        is_ >> information()(i,j);
        if (i!=j)
          information()(j,i)=information()(i,j);
      }
    if (is_.bad()) {
      //  we overwrite the information matrix with the Identity
      information().setIdentity();
    }
    return true;
  }

  bool EdgeSE3AngleAxisErrorC::write(std::ostream& os_) const {
    Vector7 meas=internal::toVectorQT(_measurement);
    for (uint16_t i = 0; i < 6; i++)
      os_ << meas[i] << " ";
    for (uint16_t i = 0; i < information().rows(); i++)
      for (uint16_t j = i; j < information().cols(); j++) {
        os_ <<  information()(i,j) << " ";
      }
    return os_.good();
  }

  bool EdgeSE3AngleAxisErrorC::setMeasurementFromState() {
    VertexSE3AngleAxisPert* from = static_cast<VertexSE3AngleAxisPert*>(_vertices[0]);
    VertexSE3AngleAxisPert* to   = static_cast<VertexSE3AngleAxisPert*>(_vertices[1]);
    Isometry3 delta = from->estimate().inverse() * to->estimate();
    setMeasurement(delta);
    return true;
  }

  void EdgeSE3AngleAxisErrorC::initialEstimate(const OptimizableGraph::VertexSet& from_,
                                               OptimizableGraph::Vertex* /*to_*/) {
    VertexSE3AngleAxisPert* from = static_cast<VertexSE3AngleAxisPert*>(_vertices[0]);
    VertexSE3AngleAxisPert* to   = static_cast<VertexSE3AngleAxisPert*>(_vertices[1]);
    if (from_.count(from) > 0) {
      to->setEstimate(from->estimate() * _measurement);
    } else {
      from->setEstimate(to->estimate() * _inverse_measurement);
    }
  }

  void EdgeSE3AngleAxisErrorC::computeError() {
    VertexSE3AngleAxisPert* from = dynamic_cast<VertexSE3AngleAxisPert*>(_vertices[0]);
    VertexSE3AngleAxisPert* to   = dynamic_cast<VertexSE3AngleAxisPert*>(_vertices[1]);
    Isometry3 delta = _inverse_measurement * from->estimate().inverse() * to->estimate();
    _error=internal::toVectorSAA(delta);
  }


#ifdef G2O_HAVE_OPENGL
  EdgeSE3AngleAxisErrorCDrawAction::EdgeSE3AngleAxisErrorCDrawAction() :
      DrawAction(typeid(EdgeSE3AngleAxisErrorC).name()) {
    //ia parent ctor
  }

  HyperGraphElementAction* EdgeSE3AngleAxisErrorCDrawAction::operator()(HyperGraph::HyperGraphElement* element,
                                                                        HyperGraphElementAction::Parameters* params) {
    if (typeid(*element).name()!=_typeName)
      return 0;
    refreshPropertyPtrs(params);
    if (! _previousParams)
      return this;

    if (_show && !_show->value())
      return this;

    EdgeSE3AngleAxisErrorC* e =  static_cast<EdgeSE3AngleAxisErrorC*>(element);
    VertexSE3AngleAxisPert* from = static_cast<VertexSE3AngleAxisPert*>(e->vertices()[0]);
    VertexSE3AngleAxisPert* to   = static_cast<VertexSE3AngleAxisPert*>(e->vertices()[1]);
    if (! from || ! to)
      return this;
    glColor3f(POSE_EDGE_COLOR);
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glBegin(GL_LINES);
    glVertex3f((float)from->estimate().translation().x(),
               (float)from->estimate().translation().y(),
               (float)from->estimate().translation().z());
    glVertex3f((float)to->estimate().translation().x(),
               (float)to->estimate().translation().y(),
               (float)to->estimate().translation().z());
    glEnd();
    glPopAttrib();
    return this;
  }
#endif


} /* namespace srrg2_core */
