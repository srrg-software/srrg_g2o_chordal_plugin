#pragma once

#include "g2o/core/base_binary_edge.h"
#include "g2o/types/slam3d/g2o_types_slam3d_api.h"
#include "vertex_se3_euler_pert.h" //ia vertex se3 euler angles

namespace g2o {

  /**
    * \brief Edge between two 3D pose vertices (with euler angle based increment)
    *
    * The transformation between the two vertices is given as an Isometry3.
    * If z denotes the measurement, then the error function is given as follows:
    * t2v_angleAxis(z^-1 * x_i^-1 * x_j)
    */
  class G2O_TYPES_SLAM3D_API EdgeSE3AngleAxisErrorA :
    public BaseBinaryEdge<6, Isometry3, VertexSE3EulerPert, VertexSE3EulerPert> {
  public:
    //! @brief ctor
    EdgeSE3AngleAxisErrorA();

    //! @brief read/write in a stream
    virtual bool read(std::istream& is);
    virtual bool write(std::ostream& os) const;

    //! @brief computes the error as t2v_angleAxis(Z^-1 \hatZ^-1)
    virtual void computeError();

    //! @brief computes the right jacobians
    // void linearizeOplus();

    void setMeasurement(const Isometry3& meas) {
      _measurement = meas;
      _inverse_measurement = meas.inverse();
    }

    virtual bool setMeasurementFromState();

    virtual number_t initialEstimatePossible(const OptimizableGraph::VertexSet& /*from*/,
                                             OptimizableGraph::Vertex* /*to*/) {
      return 1.;
    }

    virtual void initialEstimate(const OptimizableGraph::VertexSet& /*from_*/,
                                 OptimizableGraph::Vertex* /*to*/);

  protected:
    Isometry3 _inverse_measurement;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };


#ifdef G2O_HAVE_OPENGL
  //! @brief visualization
  class G2O_TYPES_SLAM3D_API EdgeSE3AngleAxisErrorADrawAction : public DrawAction {
  public:
    EdgeSE3AngleAxisErrorADrawAction();
    virtual HyperGraphElementAction* operator()(HyperGraph::HyperGraphElement* element,
                                                HyperGraphElementAction::Parameters* params);
  };
#endif

} /* namespace g2o */

