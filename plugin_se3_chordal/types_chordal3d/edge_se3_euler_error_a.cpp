#include "edge_se3_euler_error_a.h"

#ifdef G2O_HAVE_OPENGL
#include "g2o/stuff/opengl_wrapper.h"
#include "g2o/stuff/opengl_primitives.h"
#endif

namespace g2o {

  EdgeSE3EulerErrorA::EdgeSE3EulerErrorA():
      BaseBinaryEdge<6, Isometry3, VertexSE3EulerPert, VertexSE3EulerPert>() {
    _inverse_measurement.setIdentity();
    _information.setIdentity();
  }

  bool EdgeSE3EulerErrorA::read(std::istream& is_) {
    //ia measurement
    Vector7 meas = Vector7::Zero();
    for (uint16_t i = 0; i < 7; i++)
      is_ >> meas[i];

    // normalize the quaternion to recover numerical precision lost by storing as human readable text
    Vector4::MapType(meas.data()+3).normalize();

    setMeasurement(internal::fromVectorQT(meas));
    if (is_.bad()) {
      return false;
    }

    //ia information matrix
    for ( int i=0; i<information().rows() && is_.good(); i++)
      for (int j=i; j<information().cols() && is_.good(); j++){
        is_ >> information()(i,j);
        if (i!=j)
          information()(j,i)=information()(i,j);
      }
    if (is_.bad()) {
      //  we overwrite the information matrix with the Identity
      information().setIdentity();
    }
    return true;
  }

  bool EdgeSE3EulerErrorA::write(std::ostream& os_) const {
    Vector7 meas=internal::toVectorQT(_measurement);
    for (uint16_t i = 0; i < 7; i++)
      os_ << meas[i] << " ";
    for (uint16_t i = 0; i < information().rows(); i++)
      for (uint16_t j = i; j < information().cols(); j++) {
        os_ <<  information()(i,j) << " ";
      }
    return os_.good();
  }

  bool EdgeSE3EulerErrorA::setMeasurementFromState() {
    VertexSE3EulerPert* from = static_cast<VertexSE3EulerPert*>(_vertices[0]);
    VertexSE3EulerPert* to   = static_cast<VertexSE3EulerPert*>(_vertices[1]);
    Isometry3 delta = from->estimate().inverse() * to->estimate();
    setMeasurement(delta);
    return true;
  }

  void EdgeSE3EulerErrorA::initialEstimate(const OptimizableGraph::VertexSet& from_,
                                           OptimizableGraph::Vertex* /*to_*/) {
    VertexSE3EulerPert* from = static_cast<VertexSE3EulerPert*>(_vertices[0]);
    VertexSE3EulerPert* to = static_cast<VertexSE3EulerPert*>(_vertices[1]);
    if (from_.count(from) > 0) {
      to->setEstimate(from->estimate() * _measurement);
    } else {
      from->setEstimate(to->estimate() * _inverse_measurement);
    }
  }

  void EdgeSE3EulerErrorA::computeError() {
    VertexSE3EulerPert* from = dynamic_cast<VertexSE3EulerPert*>(_vertices[0]);
    VertexSE3EulerPert* to   = dynamic_cast<VertexSE3EulerPert*>(_vertices[1]);
    Isometry3 delta = _inverse_measurement * from->estimate().inverse() * to->estimate();
    _error=internal::toVectorET(delta);
  }


#ifdef G2O_HAVE_OPENGL
  EdgeSE3EulerErrorADrawAction::EdgeSE3EulerErrorADrawAction() : DrawAction(typeid(EdgeSE3EulerErrorA).name()) {
    //ia parent ctor
  }

  HyperGraphElementAction* EdgeSE3EulerErrorADrawAction::operator()(HyperGraph::HyperGraphElement* element,
                                                                    HyperGraphElementAction::Parameters* params) {
    if (typeid(*element).name()!=_typeName)
      return 0;
    refreshPropertyPtrs(params);
    if (! _previousParams)
      return this;

    if (_show && !_show->value())
      return this;

    EdgeSE3EulerErrorA* e =  static_cast<EdgeSE3EulerErrorA*>(element);
    VertexSE3EulerPert* fromEdge = static_cast<VertexSE3EulerPert*>(e->vertices()[0]);
    VertexSE3EulerPert* toEdge   = static_cast<VertexSE3EulerPert*>(e->vertices()[1]);
    if (! fromEdge || ! toEdge)
      return this;
    glColor3f(POSE_EDGE_COLOR);
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glBegin(GL_LINES);
    glVertex3f((float)fromEdge->estimate().translation().x(),(float)fromEdge->estimate().translation().y(),(float)fromEdge->estimate().translation().z());
    glVertex3f((float)toEdge->estimate().translation().x(),(float)toEdge->estimate().translation().y(),(float)toEdge->estimate().translation().z());
    glEnd();
    glPopAttrib();
    return this;
  }
#endif

} /* namespace g2o */

















