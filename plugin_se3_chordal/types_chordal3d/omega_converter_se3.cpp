#include "omega_converter_se3.h"

namespace g2o {

  //! @brief convert omega from quaternion to euler
  Matrix6 OmegaConverterSE3::convertOmegaQuaternion2Euler(const Vector6& mean_quaternion_,
                                                          const Matrix6& omega_quaternion_) {
    Matrix6 omega_euler = Matrix6::Identity();

    //ia do stuff
    const Matrix6 sigma_quaternion = omega_quaternion_.inverse();

    Vector6 mean_euler = Vector6::Zero();
    Matrix6 sigma_euler = Matrix6::Zero();

    SigmaPoint6Vector sigma_points_quaternion;
    SigmaPoint6Vector sigma_points_euler;

    sampleUnscented(sigma_points_quaternion, mean_quaternion_, sigma_quaternion);
    const size_t& sample_size = sigma_points_quaternion[0]._sample.size();

    int k = 1;
    for (size_t i = 0; i < sample_size; ++i) {
      int sample_plus_idx = k++;
      int sample_minus_idx = k++;


      const SigmaPoint6& sigma_point_quaternion_plus = sigma_points_quaternion[sample_plus_idx];
      const SigmaPoint6& sigma_point_quaternion_minus = sigma_points_quaternion[sample_minus_idx];

      const Vector6& sample_quaternion_plus = sigma_point_quaternion_plus._sample;
      const Vector6& sample_quaternion_minus = sigma_point_quaternion_minus._sample;

      Isometry3 T_plus = internal::fromVectorMQT(sample_quaternion_plus);
      Isometry3 T_minus = internal::fromVectorMQT(sample_quaternion_minus);

      Vector6 sample_euler_plus = internal::toVectorET(T_plus);
      Vector6 sample_euler_minus = internal::toVectorET(T_minus);

      SigmaPoint6 sigma_point_euler_plus(sample_euler_plus,
          sigma_point_quaternion_plus._wi,
          sigma_point_quaternion_plus._wp);
      SigmaPoint6 sigma_point_euler_minus(sample_euler_minus,
          sigma_point_quaternion_minus._wi,
          sigma_point_quaternion_minus._wp);
      sigma_points_euler.push_back(sigma_point_euler_plus);
      sigma_points_euler.push_back(sigma_point_euler_minus);
    }

    reconstructGaussian(mean_euler, sigma_euler, sigma_points_euler);
    omega_euler = sigma_euler.inverse();

/*
    std::cerr << "OmegaConverterSE3::convertOmegaQuaternion2Euler|original mean and omega [quaternion] : "
              << CL_GREEN(mean_quaternion_.transpose()) << "\n"
              << CL_GREEN(omega_quaternion_) << "\n";

    std::cerr << "OmegaConverterSE3::convertOmegaQuaternion2Euler|converted mean and omega [euler]     : "
              << CL_YELLOW(mean_euler.transpose()) << "\n"
              << CL_YELLOW(omega_euler) << "\n";

    std::cin.get();
*/

    return omega_euler;
  }

  //! @brief convert omega from euler to quaternion
  Matrix6 OmegaConverterSE3::convertOmegaEuler2Quaternion(const Vector6& mean_euler_,
      const Matrix6& omega_euler_) {

    //ia do stuff
    const Matrix6 sigma_euler = omega_euler_.inverse();

    Vector6 mean_quaternion = Vector6::Zero();
    Matrix6 sigma_quaternion = Matrix6::Zero();
    Matrix6 omega_quaternion = Matrix6::Zero();

    SigmaPoint6Vector sigma_points_euler; //in
    SigmaPoint6Vector sigma_points_quaternion; //out

    sampleUnscented(sigma_points_euler, mean_euler_, sigma_euler);
    const size_t& sample_size = sigma_points_euler[0]._sample.size();

    int k = 1;
    for (size_t i = 0; i < sample_size; ++i) {
      int sample_plus_idx = k++;
      int sample_minus_idx = k++;

      const SigmaPoint6& sigma_point_euler_plus = sigma_points_euler[sample_plus_idx];
      const SigmaPoint6& sigma_point_euler_minus = sigma_points_euler[sample_minus_idx];

      const Vector6& sample_euler_plus = sigma_point_euler_plus._sample;
      const Vector6& sample_euler_minus = sigma_point_euler_minus._sample;

      const Isometry3 T_plus = internal::fromVectorET(sample_euler_plus);
      const Isometry3 T_minus = internal::fromVectorET(sample_euler_minus);

      const Vector6 sample_quaternion_plus = internal::toVectorMQT(T_plus);
      const Vector6 sample_quaternion_minus = internal::toVectorMQT(T_minus);

      SigmaPoint6 sigma_point_quaternion_plus(sample_quaternion_plus,
          sigma_point_euler_plus._wi,
          sigma_point_euler_plus._wp);
      SigmaPoint6 sigma_point_quaternion_minus(sample_quaternion_minus,
          sigma_point_euler_plus._wi,
          sigma_point_euler_plus._wp);
      sigma_points_quaternion.push_back(sigma_point_quaternion_plus);
      sigma_points_quaternion.push_back(sigma_point_quaternion_minus);
    }

    reconstructGaussian(mean_quaternion, sigma_quaternion, sigma_points_quaternion);
    omega_quaternion = sigma_quaternion.inverse();

/*
    std::cerr << "OmegaConverterSE3::convertOmegaEuler2Quaternion|original mean and omega [euler]      : "
              << CL_GREEN(mean_euler_.transpose()) << "\n"
              << CL_GREEN(omega_euler_) << "\n";

    std::cerr << "OmegaConverterSE3::convertOmegaEuler2Quaternion|converted mean and omega [quaternion] : "
              << CL_YELLOW(mean_quaternion.transpose()) << "\n"
              << CL_YELLOW(omega_quaternion) << "\n";

    std::cin.get();
*/

    return omega_quaternion;
  }

  //! @brief convert omega from quaternion to chordal
  Matrix12 OmegaConverterSE3::convertOmegaQuaternion2Chordal(const Vector6& mean_quaternion_,
      const Matrix6& omega_quaternion_,
      const float& thresh_) {
    Matrix12 omega_chordal = Matrix12::Identity();

    //ia do stuff
    //ia computing sigma_12D
    const Matrix6 src_sigma = omega_quaternion_.inverse();
    const Isometry3 T_mean = internal::fromVectorMQT(mean_quaternion_);

    Vector12 mean_chordal = Vector12::Zero();
    Matrix12 sigma_chordal = Matrix12::Zero();

    SigmaPoint6Vector sigma_points_quaternion;
    SigmaPoint12Vector sigma_points_chordal;

    Vector6 zero_mean = Vector6::Zero();
    if (!sampleUnscented(sigma_points_quaternion, zero_mean, src_sigma))
      throw std::runtime_error("convertOmegaQuaternion2Chordal|bad things happened while sampling");

    const size_t& sample_size = mean_quaternion_.size();

    int k = 1;
    for (size_t i = 0; i < sample_size; ++i) {
      int sample_plus_idx = k++;
      int sample_minus_idx = k++;

      const Vector6& sample_quaternion_plus = sigma_points_quaternion[sample_plus_idx]._sample;
      const Vector6& sample_quaternion_minus = sigma_points_quaternion[sample_minus_idx]._sample;

      Isometry3 T_plus = internal::fromVectorMQT(sample_quaternion_plus);
      Isometry3 T_minus = internal::fromVectorMQT(sample_quaternion_minus);

      Vector12 sample_chordal_plus = internal::toFlatten(T_mean*T_plus);
      Vector12 sample_chordal_minus = internal::toFlatten(T_mean*T_minus);

      SigmaPoint12 point_chordal_plus(sample_chordal_plus, sigma_points_quaternion[sample_plus_idx]._wi, sigma_points_quaternion[sample_plus_idx]._wp);
      SigmaPoint12 point_chordal_minus(sample_chordal_minus, sigma_points_quaternion[sample_minus_idx]._wi, sigma_points_quaternion[sample_minus_idx]._wp);
      sigma_points_chordal.push_back(point_chordal_plus);
      sigma_points_chordal.push_back(point_chordal_minus);
    }

    reconstructGaussian(mean_chordal, sigma_chordal, sigma_points_chordal);

    //ia reconditioning the new covariance
    Matrix12 conditioned_sigma_chordal = Matrix12::Zero();
    Eigen::JacobiSVD<Matrix12> svd(sigma_chordal, Eigen::ComputeThinU | Eigen::ComputeThinV);
    double conditioned_eigenvalue = 1.0;
    for (int i = 0; i < 12; ++i) {
      //! best
      if (svd.singularValues()(i,i) < thresh_) {
        conditioned_eigenvalue = svd.singularValues()(i,i) + thresh_;
      } else {
        conditioned_eigenvalue = svd.singularValues()(i,i);
      }
      conditioned_sigma_chordal.noalias() += conditioned_eigenvalue *
          svd.matrixU().col(i) *
          svd.matrixU().col(i).transpose();
    }

    omega_chordal = conditioned_sigma_chordal.inverse();

/*
    std::cerr << "OmegaConverterSE3::convertOmegaQuaternion2Chordal|original mean and omega [quaternion] : "
              << CL_GREEN(mean_quaternion_.transpose()) << "\n"
              << CL_GREEN(omega_quaternion_) << "\n";

    std::cerr << "OmegaConverterSE3::convertOmegaQuaternion2Chordal|converted mean and omega [chordal]   : "
              << CL_YELLOW(mean_chordal.transpose()) << "\n"
              << CL_YELLOW(omega_chordal) << "\n";

    std::cin.get();
*/


    return omega_chordal;

  }

  //! @brief convert omega from chordal to quaternion
  Matrix6 OmegaConverterSE3::convertOmegaChordal2Quaternion(const Vector12& mean_chordal_,
      const Matrix12& omega_chordal_) {
    const Matrix12 sigma_chordal = omega_chordal_.inverse();

    Vector6 mean_quaternion = Vector6::Zero();
    Matrix6 sigma_quaternion = Matrix6::Zero();
    Matrix6 omega_quaternion = Matrix6::Zero();

    SigmaPoint12Vector sigma_points_chordal;
    SigmaPoint6Vector sigma_points_quaternion;

    sampleUnscented(sigma_points_chordal, mean_chordal_, sigma_chordal);

    const size_t& sample_size = sigma_points_chordal[0]._sample.size();

    int k = 1;
    for (size_t i = 0; i < sample_size; ++i) {
      int sample_plus_idx = k++;
      int sample_minus_idx = k++;

      const SigmaPoint12& sigma_point_chordal_plus = sigma_points_chordal[sample_plus_idx];
      const SigmaPoint12& sigma_point_chordal_minus = sigma_points_chordal[sample_minus_idx];

      const Vector12& sample_chordal_plus = sigma_point_chordal_plus._sample;
      const Vector12& sample_chordal_minus = sigma_point_chordal_minus._sample;

      Isometry3 T_plus = internal::fromFlatten(sample_chordal_plus, true);
      Isometry3 T_minus = internal::fromFlatten(sample_chordal_minus, true);

      Vector6 sample_quaternion_plus = internal::toVectorMQT(T_plus);
      Vector6 sample_quaternion_minus = internal::toVectorMQT(T_minus);

      SigmaPoint6 sigma_point_quaternion_plus(sample_quaternion_plus,
          sigma_point_chordal_plus._wi,
          sigma_point_chordal_plus._wp);
      SigmaPoint6 sigma_point_quaternion_minus(sample_quaternion_minus,
          sigma_point_chordal_minus._wi,
          sigma_point_chordal_minus._wp);
      sigma_points_quaternion.push_back(sigma_point_quaternion_plus);
      sigma_points_quaternion.push_back(sigma_point_quaternion_minus);
    }

    reconstructGaussian(mean_quaternion, sigma_quaternion, sigma_points_quaternion);
    omega_quaternion = sigma_quaternion.inverse();

/*
    std::cerr << "OmegaConverterSE3::convertOmegaQuaternion2Chordal|original mean and omega [chordal]     : "
              << CL_GREEN(mean_chordal_.transpose()) << "\n"
              << CL_GREEN(omega_chordal_) << "\n";

    std::cerr << "OmegaConverterSE3::convertOmegaQuaternion2Chordal|converted mean and omega [quaternion] : "
              << CL_YELLOW(mean_quaternion.transpose()) << "\n"
              << CL_YELLOW(omega_quaternion) << "\n";

    std::cin.get();
*/

    return omega_quaternion;

  }


  //! @brief convert omega from quaternion to scaled angle axis
  Matrix6 OmegaConverterSE3::convertOmegaQuaternion2AngleAxis(const Vector6& /*mean_quaternion_*/,
                                                              const Matrix6& /*omega_quaternion_*/) {
    Matrix6 omega_angle_axis = Matrix6::Identity();

    //ia do stuff

    return omega_angle_axis;
  }

  //! @brief convert omega from scaled angle axis to quaternion
  Matrix6 OmegaConverterSE3::convertOmegaAngleAxis2Quaternion(const Vector6& /*mean_angle_axis_*/,
                                                              const Matrix6& /*omega_angle_axis_*/) {
    Matrix6 omega_quaternion = Matrix6::Identity();

    //ia do stuff

    return omega_quaternion;
  }

} /* namespace g2o */
