#include "vertex_se3_euler_pert.h"

#include "g2o/core/factory.h"
#include "g2o/core/cache.h"

#ifdef G2O_HAVE_OPENGL
#include "g2o/stuff/opengl_wrapper.h"
#include "g2o/stuff/opengl_primitives.h"
#endif


using namespace Eigen;

namespace g2o {

  VertexSE3EulerPert::VertexSE3EulerPert() :
    BaseVertex<6, Isometry3>(),
    _num_oplus_calls(0) {
    setToOriginImpl();
    updateCache();
  }

  bool VertexSE3EulerPert::read(std::istream& is) {
    Vector7 est = Vector7::Zero();
    for (size_t i = 0; i < 7; i++)
      is  >> est[i];
    setEstimate(internal::fromVectorQT(est));
    return true;
  }

  bool VertexSE3EulerPert::write(std::ostream& os) const {
    Vector7 est = internal::toVectorQT(_estimate);
    for (size_t i = 0; i < 7; i++)
      os << est[i] << " ";
    return os.good();
  }

#ifdef G2O_HAVE_OPENGL
  VertexSE3EulerPertDrawAction::VertexSE3EulerPertDrawAction(): DrawAction(typeid(VertexSE3EulerPert).name()) {
    _cacheDrawActions = 0;
  }

  bool VertexSE3EulerPertDrawAction::refreshPropertyPtrs(HyperGraphElementAction::Parameters* params_) {
    if (!DrawAction::refreshPropertyPtrs(params_))
      return false;
    if (_previousParams){
      _triangleX = _previousParams->makeProperty<FloatProperty>(_typeName + "::TRIANGLE_X", .2f);
      _triangleY = _previousParams->makeProperty<FloatProperty>(_typeName + "::TRIANGLE_Y", .05f);
    } else {
      _triangleX = 0;
      _triangleY = 0;
    }
    return true;
  }

  HyperGraphElementAction* VertexSE3EulerPertDrawAction::operator()(HyperGraph::HyperGraphElement* element, 
                                                                HyperGraphElementAction::Parameters* params_){
    if (typeid(*element).name()!=_typeName)
      return 0;
    initializeDrawActionsCache();
    refreshPropertyPtrs(params_);

    if (! _previousParams)
      return this;
    
    if (_show && !_show->value())
      return this;

    VertexSE3EulerPert* that = static_cast<VertexSE3EulerPert*>(element);

    glColor3f(POSE_VERTEX_COLOR);
    glPushMatrix();
    glMultMatrixd(that->estimate().matrix().cast<double>().eval().data());
    opengl::drawArrow2D(_triangleX->value(), _triangleY->value(), _triangleX->value()*.3f);
    drawCache(that->cacheContainer(), params_);
    drawUserData(that->userData(), params_);
    glPopMatrix();
    return this;
  }
#endif

}
